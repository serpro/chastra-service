package chastra;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import chastra.auditoria.AuditoriaRequisicao;
import chastra.auditoria.AuditoriaRequisicaoRepository;
import chastra.exception.MensagemDeErroInterno;
import chastra.http.Headers;
import io.restassured.response.Response;

/**
 * Demonstra o padrão tracing.
 * 
 * Para essa demonstração é preciso antes subir o serviço pelo ChastraServiceApplication.
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DemonstracaoPadraoTracingTest extends ApiTest {

	@Autowired
	private AuditoriaRequisicaoRepository auditoriaRequisicaoRepository;

	@BeforeEach
	void setup() {
		auditoriaRequisicaoRepository.deleteAll();
	}

	@Test
	void trace_id_retornado_em_erro_500_aparece_em_log_e_na_auditoria() {
		
		Response response = requisicao().get(urlErroInterno());
		assertStatusCode500(response);
		String body = response.getBody().asString();
		assertThat(body).containsIgnoringCase("traceId");
		MensagemDeErroInterno msgErro = response.getBody().as(MensagemDeErroInterno.class);
		
		System.out.println("  >>> Response body retornado: " + body);
		System.out.println("  >>> Trace ID presente no response body: " + msgErro.getTraceId());
		System.out.println("  >>> Este trace ID pode ser encontrado no log registrando a exceção que ocorreu nessa chamada.");
		
		AuditoriaRequisicao ultimaAuditoria = auditoriaRequisicaoRepository.findTopByOrderByInicioOperacaoDesc();
		assertThat(ultimaAuditoria).isNotNull();

		System.out.println("  >>> Esse mesmo trace ID está presente no registro de auditoria correspondente.");
		System.out.println("  >>> Trace ID do registro de auditoria: " + ultimaAuditoria.getTraceId());
		assertThat(ultimaAuditoria.getTraceId()).isEqualTo(msgErro.getTraceId());
	}
	
	@Test
	void propagar_trace_id_em_invocacoes_rest() {
		
		System.out.println("  >>> Vamos agora invocar a operação A, que por sua vez invocará a operação B. Cada operação deixará uma mensagem no log.");
		System.out.println("  >>> Além disso, a operação A propagará o trace ID por ela gerada para a operação B por meio do header HTTP Trace-ID.");
		
		Response response = requisicao().get(urlOperacaoA());
		assertStatusCodeSemProblema(response);
		String mensagens = response.getBody().asString();
		
		System.out.println(" >>> Repare que as mensagens deixadas nos logs (" + mensagens + ") são logadas com o mesmo trace ID.");
		
		System.out.println(" >>> Vamos agora fazer uma nova invocação à operação A.");
		
		response = requisicao().get(urlOperacaoA());
		assertStatusCodeSemProblema(response);
		
		System.out.println(" >>> Note que o trace ID usado nas duas novas entrada no log agora foi outro.");
	}
	
	@Test
	void utilizar_trace_id_enviado_pelo_cliente() {
		
		String traceIdEnviado = UUID.randomUUID().toString().replace("-", "");
		
		System.out.println("  >>> Faremos uma requisição http passando o seguinte trace ID no header Trace-ID: " + traceIdEnviado);

		Response response = requisicao().header(Headers.TRACE_ID, traceIdEnviado).get(urlErroInterno());
		assertStatusCode500(response);
		MensagemDeErroInterno msgErro = response.getBody().as(MensagemDeErroInterno.class);
		String traceIdRetornado = msgErro.getTraceId();
		
		assertThat(traceIdRetornado).isEqualTo(traceIdEnviado);
		System.out.println("  >>> Veja que o trace ID retornado (" + traceIdRetornado + " é o mesmo que o trace ID enviado. Esse mesmo trace ID também estará presente no log correspondente ao erro ocorrido nessa requisição.");
	}
	
	@Test
	void paineis_origem_requisicoes() {
		
		System.out.println("  >>> Faremos agora algumas requisições para observar no Grafana a utilização dos headers Client-Application-Name e Client-Chain.");
		
		System.out.println("  >>> Primeiramente faremos uma salva de requisições para esquentar os motores do Prometheus/Grafana (por alguma questão de condições de contorno, as primeiras requisições podem não aparecer legal no gráfico).");
		primeiraSalva();
		
		System.out.println("  >>> Agora sim... fazendo 5 requisições do cliente X para a operação B...");
		for (int i=0; i<5; i++) {
			given().header(Headers.CLIENT_APPLICATION_NAME, "cliente-x").get(url8080OperacaoB());
		}
		System.out.println("  >>> Fazendo 10 requisições do cliente X para a operação A...");
		for (int i=0; i<10; i++) {
			given().header(Headers.CLIENT_APPLICATION_NAME, "cliente-x").get(url8080OperacaoA());
		}
		System.out.println("  >>> Fazendo 40 requisições do cliente Y para a operação B...");
		for (int i=0; i<40; i++) {
			given().header(Headers.CLIENT_APPLICATION_NAME, "cliente-y").get(url8080OperacaoB());
		}
		System.out.println("  >>> Fazendo 20 requisições do cliente Y para a operação A...");
		for (int i=0; i<20; i++) {
			given().header(Headers.CLIENT_APPLICATION_NAME, "cliente-y").get(url8080OperacaoA());
		}
		
		System.out.println("  >>> Agora vamos dar um tempo para que a coleta de métricas seja executada.");
		pausa();
																						                     
		System.out.println("  >>> Agora você pode verificar o dashboard 'Origem das Requisições' no Grafana (http://localhost:3000/d/e2cac544-f007-4ada-b135-d4622a2541a0/chastra-tracing, user/passwd: chastra/chastra).");
		System.out.println("  >>> Esperamos observar no painel de requisições por serviço cliente que: ");
		System.out.println("  >>>   A operação B foi invocada cerca de 5 vezes pelo cliente X.");
		System.out.println("  >>>   A operação B foi invocada cerca de 40 vezes pelo cliente Y.");
		System.out.println("  >>>   A operação B foi invocada cerca de 30 vezes pelo Chastra Service.");
		System.out.println("  >>>   A operação A foi invocada cerca de 10 vezes pelo cliente X.");
		System.out.println("  >>>   A operação A foi invocada cerca de 20 vezes pelo cliente Y.");

		System.out.println("  >>> Esperamos observar no painel de requisições por serviço de borda que: ");
		System.out.println("  >>>   A operação B foi invocada cerca de 15 vezes a partir do serviço de borda X.");
		System.out.println("  >>>   A operação B foi invocada cerca de 60 vezes a partir do serviço de borda Y.");
		System.out.println("  >>>   A operação A foi invocada cerca de 10 vezes a partir do serviço de borda X.");
		System.out.println("  >>>   A operação A foi invocada cerca de 20 vezes a partir do serviço de borda Y.");
		
		System.out.println("  >>>   Obs: note que devido a questões de aproximação intrínsicas ao Grafana e à realização da primeira salva, os valores não serão exatos.");
	}

	// Obs: o Prometheus pode levar um tempinho para coletar as métricas, por isso vamos invocar o serviço na porta 8080.
	// O serviço subido pelo teste, que roda na random port, é derrubado logo que o teste acaba e não dá tempo de o Prometheus coletar a métrica.

	private void primeiraSalva() {
		given().header(Headers.CLIENT_APPLICATION_NAME, "cliente-x").get(url8080OperacaoB());
		given().header(Headers.CLIENT_APPLICATION_NAME, "cliente-x").get(url8080OperacaoA());
		given().header(Headers.CLIENT_APPLICATION_NAME, "cliente-y").get(url8080OperacaoB());
		given().header(Headers.CLIENT_APPLICATION_NAME, "cliente-y").get(url8080OperacaoA());
		pausa();
	}

	private String url8080OperacaoB() {
		return urlOperacaoB().replaceAll(":\\d+", ":8080");
	}

	private String url8080OperacaoA() {
		return urlOperacaoA().replaceAll(":\\d+", ":8080");
	}
	
	private void pausa() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	
	@Test
	void obter_erro_se_nao_mandar_header_client_application_name() {

		Response response = given().header(Headers.CLIENT_APPLICATION_NAME, "").get(urlOperacaoA());
		assertStatusCode401(response);
		assertThat(response.getBody().asString()).containsIgnoringCase("header").contains(Headers.CLIENT_APPLICATION_NAME);
	}

}
