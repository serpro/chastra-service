package chastra;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import chastra.album.domain.NovoAlbum;
import chastra.album.infra.AlbumRepository;
import chastra.dados.MassaDeDados;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/**
 * Demonstra o padrão monitoração para o negócio.
 * 
 * Para essa demonstração é preciso antes subir o serviço pelo ChastraServiceApplication.
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DemonstracaoPadraoMonitoracaoParaONegocioTest extends ApiTest {

	@Autowired
	private AlbumRepository albumRepository;
	
	@Autowired
	private MassaDeDados massa;
	
	private List<NovoAlbum> novosAlbuns = new ArrayList<>();

	@BeforeEach
	void setup() {
		
		albumRepository.deleteAll();
		
		novosAlbuns.add(massa.album1());
		novosAlbuns.add(massa.album2());
		novosAlbuns.add(massa.album3());
		novosAlbuns.add(massa.album4());
		novosAlbuns.add(massa.album5());
		novosAlbuns.add(massa.album6());
		novosAlbuns.add(massa.album7());
		novosAlbuns.add(massa.album8());
		novosAlbuns.add(massa.album9());
		novosAlbuns.add(massa.album10());
	}

	@Test
	void requisicoes_com_sucesso_devem_gravar_auditoria_e_os_efeitos_da_transacao_de_negocio() {
		
		// Obs: o Prometheus pode levar um tempinho para coletar as métricas, por isso vamos invocar o serviço na porta 8080.
		// O serviço subido pelo teste, que roda na random port, é derrubado logo que o teste acaba e não dá tempo de o Prometheus coletar a métrica.
		String urlAlbuns8080 = urlAlbuns8080();
		System.out.println("  >>> Vamos cadastrar diversos álbuns para diversos artistas (em " + urlAlbuns8080 + ").");
		
		for (NovoAlbum novoAlbum : novosAlbuns) {
			Response response = requisicao().body(novoAlbum).contentType(ContentType.JSON).post(urlAlbuns8080);
			assertStatusCodeSemProblema(response);
			pausinha();
		}

		System.out.println("  >>> Agora você pode verificar a métrica de 'álbuns por artista' no Grafana (localhost:3000/d/f0f09daa-9b33-4f67-b40a-7ecb15c2eb9b/chastra-dashboard, user/passwd: chastra/chastra).");
	}

	private void pausinha() {
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private String urlAlbuns8080() {
		return urlAlbuns().replaceAll(":\\d+", ":8080");
	}



}
