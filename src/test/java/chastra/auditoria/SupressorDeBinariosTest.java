package chastra.auditoria;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

/**
 * Padrão supressão de binários
 *
 */
class SupressorDeBinariosTest {

    private SupressorDeBinarios supressorDeBinarios = new SupressorDeBinarios();
    
    @Test
    void suprimir_campo_base64() {
        
        String json = "{\"titulo\":\"Iron Maiden\",\"capaBase64\":\"PGRhZG9zUGRmQXRwdj48bnVtZXJ\"}";
        String jsonSemBase64 = supressorDeBinarios.suprimirBinarios(json);
        String jsonSemBase64Esperado = "{\"titulo\":\"Iron Maiden\",\"capaBase64\":\"CONTEUDO_OMITIDO_NA_AUDITORIA\"}";
        assertThat(jsonSemBase64).isEqualTo(jsonSemBase64Esperado);
    }
    
    @Test
    void suprimir_todos_os_campos_base64() {
        
        String json = "{\"titulo\":\"Iron Maiden\",\"capaBase64\":\"PGRhZG9zUGRmQXRwdj48bnVtZXJ\",\"mp3Base64\":\"PGRhZG9zUGRmQXRwdj48bnVtZXJ\"}";
        String jsonSemBase64 = supressorDeBinarios.suprimirBinarios(json);
        String fragmento1JsonSemBase64Esperado = "\"capaBase64\":\"CONTEUDO_OMITIDO_NA_AUDITORIA\"";
        String fragmento2JsonSemBase64Esperado = "\"mp3Base64\":\"CONTEUDO_OMITIDO_NA_AUDITORIA\"";
        assertThat(jsonSemBase64).contains(fragmento1JsonSemBase64Esperado).contains(fragmento2JsonSemBase64Esperado);
    }
    
    @Test
    void nao_suprimir_string_vazia() {
        
        String json = "{\"capaBase64\":\"\"}";
        String jsonSemBase64 = supressorDeBinarios.suprimirBinarios(json);
        assertThat(jsonSemBase64).isEqualTo(json);
    }
    
    @Test
    void nao_suprimir_null() {
        
        String json = "{\"capaBase64\":null}";
        String jsonSemBase64 = supressorDeBinarios.suprimirBinarios(json);
        assertThat(jsonSemBase64).isEqualTo(json);
    }
    
    @Test
    void suprimir_base64_aninhado() {
        
        String json = "{\"titulo\":\"Iron Maiden\",\"capa\":{\"autor\":\"Derek Riggs\",\"imagemBase64\":\"PGRhZG9zUGRmQXRwdj48bnVtZXJ\"}}";
        String jsonSemBase64 = supressorDeBinarios.suprimirBinarios(json);
        String fragmentoJsonSemBase64Esperado = "\"imagemBase64\":\"CONTEUDO_OMITIDO_NA_AUDITORIA\"";
        assertThat(jsonSemBase64).contains(fragmentoJsonSemBase64Esperado);
    }
    
    @Test
    void suprimir_campo_base64_de_cada_elemento_da_lista() {
        
        String json = "[{\"titulo\":\"Iron Maiden\",\"capaBase64\":\"PGRhZG9zUGRmQXRwdj48bnVtZXJ\"},{\"titulo\":\"Killers\",\"capaBase64\":\"PGRhZG9zUGRmQXRwdj48bnVtZXJ\"}]";
        String jsonSemBase64 = supressorDeBinarios.suprimirBinarios(json);
        String jsonSemBase64Esperado = "[{\"titulo\":\"Iron Maiden\",\"capaBase64\":\"CONTEUDO_OMITIDO_NA_AUDITORIA\"},{\"titulo\":\"Killers\",\"capaBase64\":\"CONTEUDO_OMITIDO_NA_AUDITORIA\"}]";
        assertThat(jsonSemBase64).isEqualTo(jsonSemBase64Esperado);
    }    
    
    @Test
    void suprimir_campos_base64_de_lista_aninhada() {
        
        String json = "{\"artista\":\"Iron Maiden\",\"albuns\":[{\"titulo\":\"Iron Maiden\",\"capaBase64\":\"PGRhZG9zUGRmQXRwdj48bnVtZXJ\"},{\"titulo\":\"Killers\",\"capaBase64\":\"PGRhZG9zUGRmQXRwdj48bnVtZXJ\"}]}";
        String jsonSemBase64 = supressorDeBinarios.suprimirBinarios(json);
        int qtdSupressoes = StringUtils.countMatches(jsonSemBase64, SupressorDeBinarios.MARCA_DE_SUPRESSAO);
		assertThat(qtdSupressoes)
				.as("Quantidade de supressões não foi conforme esperado. Json verificado: " + jsonSemBase64)
				.isEqualTo(2);
    }
    
    @Test
    void suprimir_campos_base64_de_lista_dentro_de_lista() {
        
        String json = "[[{\"fotoBandaBase64\":\"PGRhZG9zUGRmQXRwdj48bnVtZXJ\"},{\"fotoCapaBase64\":\"PGRhZG9zUGRmQXRwdj48bnVtZXJ\"}],[{\"fotoBandaBase64\":\"PGRhZG9zUGRmQXRwdj48bnVtZXJ\"},{\"fotoCapaBase64\":\"PGRhZG9zUGRmQXRwdj48bnVtZXJ\"}]]";
        String jsonSemBase64 = supressorDeBinarios.suprimirBinarios(json);
        int qtdSupressoes = StringUtils.countMatches(jsonSemBase64, SupressorDeBinarios.MARCA_DE_SUPRESSAO);
		assertThat(qtdSupressoes)
				.as("Quantidade de supressões não foi conforme esperado. Json verificado: " + jsonSemBase64)
				.isEqualTo(4);
    }
    
}
