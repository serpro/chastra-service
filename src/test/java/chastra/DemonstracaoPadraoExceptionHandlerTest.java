package chastra;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import chastra.album.api.NovaImagemCapa;
import chastra.album.domain.AlbumCriado;
import chastra.album.domain.NovoAlbum;
import chastra.dados.MassaDeDados;
import chastra.traceid.TraceIdManager;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/**
 * Demonstra o padrão exception handler.
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DemonstracaoPadraoExceptionHandlerTest extends ApiTest {

	@Autowired
	private MassaDeDados massa;
	
	@Test
	void erro_interno_deve_retornar_status_500() {
		
		Response response = requisicao().get(urlErroInterno());
		assertStatusCode500(response);
		String body = response.getBody().asString();
		
		System.out.println("  >>> Response body em caso de erro interno (500): " + body);
		
		assertThat(body).containsIgnoringCase("Erro Interno");
		assertBodyHasDataHora(body);
		assertBodyHasTraceId(body);
	}

	private void assertBodyHasDataHora(String body) {
		String errorMsg = "Response body não contém data hora, mas deveria; body = '" + body + "'";
		String DATA_HORA_REGEX = ".*\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.*";
		assertThat(body).as(errorMsg).matches(DATA_HORA_REGEX);
	}
	
	private void assertBodyHasTraceId(String body) {
		
		assertThat(body).containsIgnoringCase("Trace ID");
		String errorMsg = "Body não contém 'Trace ID', mas deveria; body = '" + body + "'";
		assertThat(body).as(errorMsg).matches(TraceIdManager.TRACE_ID_REGEX);
	}
	
	@Test
	void erro_de_negocio_deve_retornar_status_422() {
		
		Response response = requisicao().get(urlErroDeNeogcio());
		assertStatusCode422(response);
		String body = response.getBody().asString();
		
		System.out.println("  >>> Response body em caso de erro de negócio (422): " + body);

		assertThat(body).containsIgnoringCase("Erro de negócio").contains("mensagemParaUsuarioFinal");
	}
	
	@Test
	void requisicao_invalida_deve_retornar_status_400() {
		
		Response response = requisicao().get(urlRequisicaoInvalida());
		assertStatusCode400(response);
		String body = response.getBody().asString();

		System.out.println("  >>> Response body em caso de requisição inválida (400): " + body);
		
		assertThat(body).containsIgnoringCase("Requisição inválida").contains("erro");
	}
	
	@Test
	void request_body_violando_o_bean_validation_deve_retornar_erro_400() {
		
		NovoAlbum novoAlbum = massa.album1();
		novoAlbum.setArtista(null);
		Response response1 = requisicao().body(novoAlbum).contentType(ContentType.JSON).post(urlAlbuns());
		assertStatusCode400(response1);
		String body1 = response1.getBody().asString();
		
		System.out.println("  >>> Response body em caso de requisição violando o bean validation (400): " + body1);

		assertThat(body1).containsIgnoringCase("Requisição inválida").contains("erro").contains("artista")
				.contains("vazio").doesNotContain("anoLancamento");
		
		novoAlbum = massa.album1();
		Response response2 = requisicao().body(novoAlbum).contentType(ContentType.JSON).post(urlAlbuns());
		assertStatusCodeSemProblema(response2);
		AlbumCriado albumCriado = response2.getBody().as(AlbumCriado.class);
		Long idAlbumCriado = albumCriado.getIdAlbum();
		
		String base64Invalido = "AsdpjiAdde+rsd_//\\_^_^_//\\";
		NovaImagemCapa novaImagemCapa = new NovaImagemCapa(base64Invalido);
		Response response3 = requisicao().body(novaImagemCapa).contentType(ContentType.JSON).pathParam("idAlbum", idAlbumCriado).put(urlImagemCapa());
		assertStatusCode400(response3);
		String body3 = response3.getBody().asString();
		
		System.out.println("  >>> Outro response body em caso de requisição violando o bean validation (400): " + body3);
		
		assertThat(body3).containsIgnoringCase("Requisição inválida").contains("erro").contains("imagemCapaBase64")
				.contains(NovaImagemCapa.BASE64_PATTERN);
	}
}
