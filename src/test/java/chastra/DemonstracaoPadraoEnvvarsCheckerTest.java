package chastra;

import org.junit.jupiter.api.Test;

/**
 * Demonstra o padrão envvars checker.
 *
 */
class DemonstracaoPadraoEnvvarsCheckerTest {

	@Test
	void como_demonstrar() {
		
		System.out.println(" >>> Este padrão não possui demonstração automatizada.");
		System.out.println(" >>> Para demonstrá-lo: no application.properties, altere o valor da propriedade ambiente de 'local' para 'hom'.");
		System.out.println(" >>> Em seguida, inicie a aplicação.");
		System.out.println(" >>> É esperado que a aplicação não suba (na verdade suba, mas na sequência seja automaticamente derrubada).");
		System.out.println(" >>> No log deve constar a mensagem 'variáveis de ambiente não estão preenchidas'.");
		System.out.println(" >>> Isso ocorre porque no ambiente de homologação (ou produção) as variáveis que aparecem no application.properties (ex: CHASTRA_DB_HOST) devem estar preenchidas.");
	}
}
