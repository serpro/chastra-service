package chastra;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import chastra.auditoria.AuditoriaRequisicaoRepository;
import chastra.controlederajadas.api.EnvioOperacaoDemorada;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/**
 * Demonstra o padrão controle de rajadas.
 * 
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DemonstracaoPadraoControleDeRajadasTest extends ApiTest {

	@Autowired
	private AuditoriaRequisicaoRepository auditoriaRequisicaoRepository;

	private AtomicInteger counterRajadasBloqueadas;
	
	@BeforeEach
	void setup() {
		
		this.counterRajadasBloqueadas  = new AtomicInteger();
		auditoriaRequisicaoRepository.deleteAll();
	}
	
	@Test
	void controle_de_rajadas_deve_bloquear_quatro_das_cinco_requisicoes_com_corpos_similares_disparadas_simultaneamente() {

		int numThreads = 5;
		
		List<Thread> threads = new ArrayList<>(); 
		long mesmoIdArtistaParaTodasAsThreads = 1523L;
		for (int i=0; i< numThreads; i++) {
			System.out.println("  >>> Thread " + i + " invocando operação demorada com idArtista " + mesmoIdArtistaParaTodasAsThreads);
			Thread thread = new Thread(new Invocador(i, mesmoIdArtistaParaTodasAsThreads));
			thread.start();
			threads.add(thread);
		}
		
		// espera todo mundo terminar
		for (Thread thread : threads) {
			esperar(thread);
		}
		
		assertThat(counterRajadasBloqueadas.get()).isEqualTo(numThreads-1);
		System.out.println("  >>> Como todas as invocações usaram o mesmo idArtista, então tivemos "
				+ counterRajadasBloqueadas.get() + " invocações boqueadas; i.e., detectamos uma rajada!");
	}

	@Test
	void controle_de_rajadas_nao_deve_bloquear_nenhuma_requisicao_quando_requicioes_enviadas_nao_compartilham_campo_chave_em_comum() {
		
		int numThreads = 5;
		
		List<Thread> threads = new ArrayList<>(); 
		for (int i=0; i< numThreads; i++) {
			Long umIdArtistaDiferenteParaCadaThread = Long.valueOf(i);
			System.out.println("  >>> Thread " + i + " invocando operação demorada com idArtista " + umIdArtistaDiferenteParaCadaThread);
			Thread thread = new Thread(new Invocador(i, umIdArtistaDiferenteParaCadaThread));
			thread.start();
			threads.add(thread);
		}
		
		// espera todo mundo terminar
		for (Thread thread : threads) {
			esperar(thread);
		}
		
		assertThat(counterRajadasBloqueadas.get()).isZero();
		System.out.println("  >>> Como todas as invocações usaram o mesmo idArtista, então tivemos "
				+ counterRajadasBloqueadas.get() + " invocações boqueadas; i.e., nada foi bloqueado, pois não houve rajada.");

	}
	
	private class Invocador implements Runnable {

		private int numThread;
		private Long idArtista;

		public Invocador(int numThread, Long idArtista) {
			this.numThread = numThread;
			this.idArtista = idArtista;
		}

		@Override
		public void run() {

			EnvioOperacaoDemorada solicitacao = new EnvioOperacaoDemorada();
			solicitacao.setIdArtista(idArtista);
			solicitacao.setQuantidadeFans(numThread);
			
			Response response = requisicao().contentType(ContentType.JSON).body(solicitacao).
					post(urlOperacaoDemorada());
			int statusCode = response.getStatusCode();
			String responseBody = response.getBody().asString();
			if (statusCode == 422 && responseBody.contains("rajadas")) {
				counterRajadasBloqueadas.incrementAndGet();
			}
		}
	}
	
	private void esperar(Thread t) {
		try {
			t.join();
		} catch (InterruptedException e) {
			throw new IllegalStateException("Ih, deu exceção enquanto esperava a thread terminar!", e);
		}
	}

}
