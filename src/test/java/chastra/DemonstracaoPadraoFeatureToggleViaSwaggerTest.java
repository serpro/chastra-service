package chastra;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import chastra.featuretoggle.domain.FeatureToggle;
import chastra.featuretoggle.infra.FeatureToggleRepository;
import chastra.utils.DateUtils;
import io.restassured.response.Response;

/**
 * Demonstra o padrão feature toggle via swagger.
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DemonstracaoPadraoFeatureToggleViaSwaggerTest extends ApiTest {

	private static final String FUNCIONALIDADE = "excluir-album";
	
	@Autowired
	private FeatureToggleRepository featureToggleRepository;
	
	@BeforeEach
	void setup() {
		
	}

	@AfterEach
	void deixarFeatureToggleComoEstava() {
		ativarFeatureToggle();
	}

	@Test
	void deve_ativar_feature_toggle() {
		
		System.out.println("  >>> Setup: vamos deixar o feature toggle desativado.");
		desativarFeatureToggle();
		
		Response response = requisicao().get(urlFeatureToggles());
		assertStatusCodeSemProblema(response);
		FeatureToggle[] featureTogglesArray = response.getBody().as(FeatureToggle[].class);
		List<FeatureToggle> featureToggles = List.of(featureTogglesArray);
		assertThat(featureToggles.stream().filter(ft -> FUNCIONALIDADE.equals(ft.getFuncionalidade())).findFirst().get().getLigada()).isFalse();
		System.out.println("  >>> Estado da tabela de feature toggles (feature toggle " + FUNCIONALIDADE + " está desligada): " + featureToggles);
		
		System.out.println("  >>> Ativando feature toggle via API...");
		response = requisicao().queryParam("nomeFuncionalidade", FUNCIONALIDADE).post(urlAtivarFeatureToggle());
		assertStatusCodeSemProblema(response);

		response = requisicao().get(urlFeatureToggles());
		assertStatusCodeSemProblema(response);
		featureTogglesArray = response.getBody().as(FeatureToggle[].class);
		featureToggles = List.of(featureTogglesArray);
		assertThat(featureToggles.stream().filter(ft -> FUNCIONALIDADE.equals(ft.getFuncionalidade())).findFirst().get().getLigada()).isTrue();
		System.out.println("  >>> Estado da tabela de feature toggles após ativação (feature toggle " + FUNCIONALIDADE + " está ligada): " + featureToggles);
	}
	
	private void ativarFeatureToggle() {
		
		FeatureToggle featureToggle = featureToggleRepository.getByFuncionalidade(FUNCIONALIDADE);
		featureToggle.setLigada(true);
		featureToggle.setDataHoraAtivacao(DateUtils.agora());
		featureToggleRepository.save(featureToggle);
	}
	
	private void desativarFeatureToggle() {
		
		FeatureToggle featureToggle = featureToggleRepository.getByFuncionalidade(FUNCIONALIDADE);
		featureToggle.setLigada(false);
		featureToggle.setDataHoraAtivacao(null);
		featureToggleRepository.save(featureToggle);
	}

}
