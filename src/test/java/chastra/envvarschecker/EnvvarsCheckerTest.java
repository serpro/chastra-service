package chastra.envvarschecker;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

/**
 * Padrão envvars checker
 *
 */
class EnvvarsCheckerTest {

	@Test
	void test() {

		EnvvarsFinder envvarsFinder = mock(EnvvarsFinder.class);
		when(envvarsFinder.find()).thenReturn(envvarsDeclaradasNoApplicationProperties());
		EnvvarsEnvironmentStub environment = new EnvvarsEnvironmentStub(envvarsDisponiveisNoAmbiente());

		EnvvarsChecker envvarsChecker = new EnvvarsChecker(envvarsFinder, environment);

		List<String> envvarsNaoPreenchidas = envvarsChecker.envvarsNaoPreenchidas();
		assertThat(envvarsNaoPreenchidas).hasSize(2).contains("CHASTRA_DB_PORT").contains("CHASTRA_DB_PASSWORD");
	}

	private List<String> envvarsDeclaradasNoApplicationProperties() {
		return List.of("CHASTRA_DB_HOST", "CHASTRA_DB_PORT", "CHASTRA_DB_USER", "CHASTRA_DB_PASSWORD");
	}

	private Map<String, String> envvarsDisponiveisNoAmbiente() {
		return Map.of("CHASTRA_DB_HOST", "168.10.0.1", "CHASTRA_DB_USER", "chastra-user");
	}

	@Test
	void ignorar_variaveis_declaradas_na_ignore_list_do_envvars_checker() {

		String envvarParaIgnorar = "LOGGING_LEVEL";
		List<String> envvarsDeclaradas = new ArrayList<>(envvarsDeclaradasNoApplicationProperties());
		envvarsDeclaradas.add(envvarParaIgnorar);

		Map<String, String> envvarsNoAmbiente = envvarsDisponiveisNoAmbiente();
		assertThat(envvarsNoAmbiente.get(envvarParaIgnorar)).isNull();

		EnvvarsFinder envvarsFinder = mock(EnvvarsFinder.class);
		when(envvarsFinder.find()).thenReturn(envvarsDeclaradas);
		EnvvarsEnvironmentStub environment = new EnvvarsEnvironmentStub(envvarsNoAmbiente);

		EnvvarsChecker envvarsChecker = new EnvvarsChecker(envvarsFinder, environment);

		List<String> envvarsNaoPreenchidas = envvarsChecker.envvarsNaoPreenchidas();
		assertThat(envvarsNaoPreenchidas).hasSize(2).contains("CHASTRA_DB_PORT").contains("CHASTRA_DB_PASSWORD");
	}

}
