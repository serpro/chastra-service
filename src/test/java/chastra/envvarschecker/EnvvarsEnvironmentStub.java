package chastra.envvarschecker;

import java.util.Map;

/**
 * Padrão envvars checker
 *
 */
class EnvvarsEnvironmentStub extends EnvvarsEnvironment {

	private Map<String, String> envvars;
	
	public EnvvarsEnvironmentStub(Map<String, String> envvars) {
		this.envvars = envvars;
	}
	
	@Override
	String getEnvvar(String envvar) {
		return envvars.get(envvar);
	}
}
