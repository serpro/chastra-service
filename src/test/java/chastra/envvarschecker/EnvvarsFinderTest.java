package chastra.envvarschecker;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Padrão envvars checker
 *
 */
class EnvvarsFinderTest {

	@Test
	void encontrar_envvars_declaradas_no_application_properties() {
		
		EnvvarsFinder envvarsFinder = new EnvvarsFinder();
		List<String> envvars = envvarsFinder.find();
		assertThat(envvars).hasSizeGreaterThanOrEqualTo(6).contains("AMBIENTE").contains("CHASTRA_DB_HOST")
				.contains("CHASTRA_DB_PORT").contains("CHASTRA_DB_NAME").contains("CHASTRA_DB_USER")
				.contains("CHASTRA_DB_PASSWORD");
	}

}
