package chastra;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import chastra.album.domain.Album;
import chastra.album.domain.AlbumCriado;
import chastra.album.domain.NovoAlbum;
import chastra.album.infra.AlbumRepository;
import chastra.dados.MassaDeDados;
import chastra.featuretoggle.domain.FeatureToggle;
import chastra.featuretoggle.infra.FeatureToggleRepository;
import chastra.utils.DateUtils;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/**
 * Demonstra o padrão feature toggle.
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DemonstracaoPadraoFeatureToggleTest extends ApiTest {

	private static final String FUNCIONALIDADE = "excluir-album";
	
	@Autowired
	private FeatureToggleRepository featureToggleRepository;
	
	@Autowired
	private AlbumRepository albumRepository;

	@Autowired
	private MassaDeDados massa;

	@BeforeEach
	void setup() {
		albumRepository.deleteAll();
	}

	@AfterEach
	void deixarFeatureToggleComoEstava() {
		ativarFeatureToggle();
	}

	@Test
	void deve_excluir_album_quando_feature_toggle_ligado() {
		
		System.out.println("  >>> Vamos ativar o feature toggle.");
		ativarFeatureToggle();
		
		NovoAlbum novoAlbum = massa.album1();
		Response response = requisicao().body(novoAlbum).contentType(ContentType.JSON).post(urlAlbuns());
		assertStatusCodeSemProblema(response);
		Long idAlbum = response.getBody().as(AlbumCriado.class).getIdAlbum();
		
		List<Album> albunsEncontrados = find(novoAlbum);
		assertThat(albunsEncontrados).hasSize(1);
		System.out.println("  >>> Temos álbum cadastrado: " + albunsEncontrados.get(0));
		
		System.out.println("  >>> Invocando a exclusão do álbum...");
		response = requisicao().pathParam("idAlbum", idAlbum).delete(urlAlbumPorId());
		assertStatusCodeSemProblema(response);
		System.out.println("  >>> OK, status code = " + response.statusCode());

		albunsEncontrados = find(novoAlbum);
		assertThat(albunsEncontrados).isEmpty();
		System.out.println("  >>> Como o feature toggle estava ligado, o álbum foi excluído, não temos mais álbum: " + albunsEncontrados);
	}
	
	@Test
	void nao_deve_excluir_album_quando_feature_toggle_desligado() {
		
		System.out.println("  >>> Vamos desativar o feature toggle.");
		desativarFeatureToggle();
		
		NovoAlbum novoAlbum = massa.album1();
		Response response = requisicao().body(novoAlbum).contentType(ContentType.JSON).post(urlAlbuns());
		assertStatusCodeSemProblema(response);
		Long idAlbum = response.getBody().as(AlbumCriado.class).getIdAlbum();
		
		List<Album> albunsEncontrados = find(novoAlbum);
		assertThat(albunsEncontrados).hasSize(1);
		System.out.println("  >>> Temos álbum cadastrado: " + albunsEncontrados.get(0));
		
		System.out.println("  >>> Invocando a exclusão do álbum...");
		response = requisicao().pathParam("idAlbum", idAlbum).delete(urlAlbumPorId());
		assertStatusCode400(response);
		String responseBody = response.getBody().asString();
		assertThat(responseBody).containsIgnoringCase("Funcionalidade").containsIgnoringCase("não disponível");
		System.out.println("  >>> Retorno da exclusão (status code 400): " + responseBody);

		albunsEncontrados = find(novoAlbum);
		assertThat(albunsEncontrados).hasSize(1);
		System.out.println("  >>> Como o feature toggle estava desligado, ainda temos álbum cadastrado: " + albunsEncontrados.get(0));
	}

	private List<Album> find(NovoAlbum novoAlbum) {
		return albumRepository.findAllByTituloAndArtista(novoAlbum.getTitulo(), novoAlbum.getArtista());
	}

	private void ativarFeatureToggle() {
		
		FeatureToggle featureToggle = featureToggleRepository.getByFuncionalidade(FUNCIONALIDADE);
		featureToggle.setLigada(true);
		featureToggle.setDataHoraAtivacao(DateUtils.agora());
		featureToggleRepository.save(featureToggle);
	}
	
	private void desativarFeatureToggle() {
		
		FeatureToggle featureToggle = featureToggleRepository.getByFuncionalidade(FUNCIONALIDADE);
		featureToggle.setLigada(false);
		featureToggle.setDataHoraAtivacao(null);
		featureToggleRepository.save(featureToggle);
	}
	
}
