package chastra.dados;

import org.springframework.stereotype.Component;

import chastra.album.domain.NovoAlbum;

@Component
public class MassaDeDados {
	
	public NovoAlbum album1() {
		return NovoAlbum.comTitulo("The Fallen King").comArtista("Frozen Crown").comAnoLancamento(2018).build();
	}
	
	public NovoAlbum album2() {
		return NovoAlbum.comTitulo("Crowned_in_Frost").comArtista("Frozen Crown").comAnoLancamento(2019).build();
	}
	
	public NovoAlbum album3() {
		return NovoAlbum.comTitulo("Winterbane").comArtista("Frozen Crown").comAnoLancamento(2021).build();
	}
	
	public NovoAlbum album4() {
		return NovoAlbum.comTitulo("Call of the North").comArtista("Frozen Crown").comAnoLancamento(2023).build();
	}
	
	public NovoAlbum album5() {
		return NovoAlbum.comTitulo("Heaven's Sewer").comArtista("Heaven's Sewer").comAnoLancamento(2022).build();
	}

	public NovoAlbum album6() {
		return NovoAlbum.comTitulo("Nevermind").comArtista("Nirvana").comAnoLancamento(1991).build();
	}
	
	public NovoAlbum album7() {
		return NovoAlbum.comTitulo("Thriller").comArtista("Michael Jackson").comAnoLancamento(1982).build();
	}
	
	public NovoAlbum album8() {
		return NovoAlbum.comTitulo("Hotel California").comArtista("The Eagles").comAnoLancamento(1976).build();
	}
	
	public NovoAlbum album9() {
		return NovoAlbum.comTitulo("A Night at the Opera").comArtista("Queen").comAnoLancamento(1975).build();
	}
	
	public NovoAlbum album10() {
		return NovoAlbum.comTitulo("A Night at the Opera").comArtista("Blind Guardian").comAnoLancamento(2002).build();
	}

}
