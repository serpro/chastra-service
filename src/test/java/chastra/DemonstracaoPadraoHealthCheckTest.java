package chastra;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import io.restassured.response.Response;

/**
 * Demonstra o Padrão health check.
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DemonstracaoPadraoHealthCheckTest extends ApiTest {

	@Test
	void heal_alive() {
		
		Response response = requisicao().get(urlHealthAlive());
		assertStatusCode200(response);
		String responseBody = response.getBody().asString();
		
		System.out.println("  >>> Retorno do /health/alive: statusCode = " + response.getStatusCode() + ", response body = '" + responseBody + "'.");
	}
	
	@Test
	void heal_integration() {
		
		Response response = requisicao().get(urlHealthIntegration());
		assertStatusCode200(response);
		String responseBody = response.getBody().asString();
		
		System.out.println("  >>> Retorno do /health/integration: statusCode = " + response.getStatusCode() + ", response body = '" + responseBody + "'.");
	}

	@Test
	void heal_integration_quando_servico_dependencia_retorna_500() {
		
		Response response = requisicao().queryParam("forcarErro500AoVerificarAcessoAoServicoRecomendacao", true)
				.get(urlHealthIntegration());
		assertStatusCode500(response);
		String responseBody = response.getBody().asString();
		
		System.out.println("  >>> Quando serviço dependência retorna 500, retorno do /health/integration: statusCode = " + response.getStatusCode() + ", response body = '" + responseBody + "'.");
	}
	
	@Test
	void heal_integration_quando_servico_dependencia_com_timeout() {
		
		Response response = requisicao().queryParam("forcarErroTimeoutAoVerificarAcessoAoServicoRecomendacao", true)
				.get(urlHealthIntegration());
		assertStatusCode500(response);
		String responseBody = response.getBody().asString();
		
		System.out.println("  >>> Quando serviço dependência dá timeout, retorno do /health/integration (note que o stacktrace é multilinha para melhor leitura no navegador): statusCode = " + response.getStatusCode() + ", response body = '" + responseBody + "'.");
		System.out.println("  >>> Fim da impressão do response body");
	}
}
