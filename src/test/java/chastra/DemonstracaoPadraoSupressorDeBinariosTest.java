package chastra;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import chastra.album.api.AlbumJson;
import chastra.auditoria.AuditoriaRequisicao;
import chastra.auditoria.AuditoriaRequisicaoRepository;
import chastra.auditoria.SupressorDeBinarios;
import io.restassured.response.Response;

/**
 * Demonstra o padrão supressão de binários.
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DemonstracaoPadraoSupressorDeBinariosTest extends ApiTest {

	private static final int _1kb = 1000;
	@Autowired
	private AuditoriaRequisicaoRepository auditoriaRequisicaoRepository;
	
	@Test
	@SuppressWarnings("unchecked")
	void campos_binarios_devem_ser_omitidos_na_auditoria() {

		// Requisição retorna body grande (>1k) pois response body contém binários
		Response response = requisicao().get(urlAlbuns());
		assertStatusCodeSemProblema(response);
		String responseBody = response.getBody().asString();
		assertThat(responseBody).hasSizeGreaterThan(_1kb);
		List<AlbumJson> albums = asList(response, AlbumJson.class);
		int qtdAlbunsRetornados = albums.size();
		assertThat(qtdAlbunsRetornados).isNotZero();

		// agora encontra auditoria correspondente à requisição acima
		AuditoriaRequisicao ultimaAuditoria = auditoriaRequisicaoRepository.findTopByOrderByInicioOperacaoDesc();
		String corpoRespostaAuditado = ultimaAuditoria.getCorpoResposta();
		
		System.out.println("  >>> Corpo resposta auditado (com binários omitidos): " + corpoRespostaAuditado);
		
		// auditoria encontrada possui campo corpoResposta pequeno (<1k)
		int qtdCamposSuprimidosNaAuditoria = StringUtils.countMatches(corpoRespostaAuditado, SupressorDeBinarios.MARCA_DE_SUPRESSAO);
		assertThat(qtdCamposSuprimidosNaAuditoria).isEqualTo(qtdAlbunsRetornados);
		assertThat(corpoRespostaAuditado).hasSizeLessThan(_1kb);
	}		

}
