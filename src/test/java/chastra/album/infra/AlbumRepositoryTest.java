package chastra.album.infra;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import chastra.album.domain.Album;

// webEnvironment colocado para evitar erro
// Caused by: org.springframework.beans.factory.NoSuchBeanDefinitionException: No qualifying bean of type 'io.micrometer.core.instrument.composite.CompositeMeterRegistry' available: expected at least 1 bean which qualifies as autowire candidate. Dependency annotations: {}
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class AlbumRepositoryTest {

	@Autowired
	private AlbumRepository albumRepository;
	
	@BeforeEach
	void setUp() {
		albumRepository.deleteAll();
	}
	
	@Test
	void test() {
		
		Album album1 = new Album();
		album1.setTitulo("Dystopia");
		album1.setArtista("Megadeth");
		album1 = albumRepository.save(album1);

		Album album2 = new Album();
		album2.setTitulo("Dystopia");
		album2.setArtista("Iced Earth");
		album2 = albumRepository.save(album2);

		Album album3 = new Album();
		album3.setTitulo("The Sark Saga");
		album3.setArtista("Iced Earth");
		album3 = albumRepository.save(album3);
		
		List<Album> albuns = albumRepository.findAllByTituloAndArtista("Dystopia", "Iced Earth");
		assertThat(albuns).hasSize(1);
		Album albumEncontrado = albuns.get(0);
		assertThat(albumEncontrado.getId()).isEqualTo(album2.getId());
	}

}
