package chastra;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import chastra.album.domain.Album;
import chastra.album.domain.NovoAlbum;
import chastra.album.infra.AlbumRepository;
import chastra.dados.MassaDeDados;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/**
 * Demonstra o padrão idempotência.
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class DemonstracaoPadraoIdempotenciaTest extends ApiTest {

	@Autowired
	private AlbumRepository albumRepository;
	
	@Autowired
	private MassaDeDados massa;

	@BeforeEach
	void setup() {
		albumRepository.deleteAll();
	}
	
	@Test
	void segunda_requisicao_deve_retornar_sucesso_mas_sem_duplicar_cadastro() {

		// primeira requisição
		NovoAlbum novoAlbum = massa.album1();
		Response response1 = requisicao().body(novoAlbum).contentType(ContentType.JSON).post(urlAlbuns());
		assertStatusCodeSemProblema(response1);
		
		// segunda requisição (sucesso também)
		Response response2 = requisicao().body(novoAlbum).contentType(ContentType.JSON).post(urlAlbuns());
		assertStatusCodeSemProblema(response2);
		
		System.out.println("  >>> Status code retornado pelo segundo envio (idêntico ao primeiro): " + response2.getStatusCode() + ". Ou seja, serviço aceita a segunda requisição em vez de retornar erro para uma melhor experiência do usuário.");

		// verificação: apenas um álbum cadastrado
		List<Album> albunsEncontrados = albumRepository.findAllByTituloAndArtista(novoAlbum.getTitulo(), novoAlbum.getArtista());
		assertThat(albunsEncontrados).hasSize(1);
		System.out.println("  >>> Contudo, apesar das duas requisições terem sido respondidas com sucesso, apenas " + albunsEncontrados.size() + " álbum foi cadastrado, o que é o desejável.");
	}
	
	@Test
	void segunda_requisicao_deve_retornar_erro_devido_a_dados_divergentes() {

		// primeira requisição (sucesso)
		NovoAlbum novoAlbum1 = massa.album1();
		Response response1 = requisicao().body(novoAlbum1).contentType(ContentType.JSON).post(urlAlbuns());
		assertStatusCodeSemProblema(response1);
		
		// segunda requisição com dados divergentes: erro
		NovoAlbum novoAlbum2 = massa.album1();
		novoAlbum2.setAnoLancamento(1970);
		assertThat(novoAlbum2.getAnoLancamento()).isNotEqualTo(novoAlbum1.getAnoLancamento());
		Response response2 = requisicao().body(novoAlbum2).contentType(ContentType.JSON).post(urlAlbuns());
		assertStatusCode422(response2);
		
		String responseBody2 = response2.getBody().asString();
		System.out.println("  >>> Status code retornado pelo segundo envio: " + response2.getStatusCode() + ". Erro foi devido ao envio de dados divergentes em relação ao primeiro envio. Response body = '" + responseBody2 + "'.");

		// verificação: apenas um álbum cadastrado
		List<Album> albunsEncontrados = albumRepository.findAllByTituloAndArtista(novoAlbum1.getTitulo(), novoAlbum1.getArtista());
		assertThat(albunsEncontrados).hasSize(1);
		System.out.println("  >>> Como a segunda requisição retornou um erro, apenas " + albunsEncontrados.size() + " álbum foi cadastrado, o que é o desejável.");
	}

}
