package chastra;


import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import chastra.album.domain.Album;
import chastra.album.domain.NovoAlbum;
import chastra.album.infra.AlbumRepository;
import chastra.auditoria.AuditoriaRequisicao;
import chastra.auditoria.AuditoriaRequisicaoRepository;
import chastra.dados.MassaDeDados;
import chastra.traceid.TraceIdManager;
import chastra.utils.DateUtils;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/**
 * Demonstra o padrão auditoria.
 * 
 * Testa também questões transacionais.
 *
 */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DemonstracaoPadraoAuditoriaTest extends ApiTest {

	@Autowired
	private AuditoriaRequisicaoRepository auditoriaRequisicaoRepository;

	@Autowired
	private AlbumRepository albumRepository;
	
	@Autowired
	private MassaDeDados massa;
	
	@BeforeEach
	void setup() {
		auditoriaRequisicaoRepository.deleteAll();
	}

	@Test
	void requisicoes_com_sucesso_devem_gravar_auditoria_e_os_efeitos_da_transacao_de_negocio() {
		
		albumRepository.deleteAll();
		
		NovoAlbum novoAlbum = massa.album1();
		Response response = requisicao().body(novoAlbum).contentType(ContentType.JSON).post(urlAlbuns());
		assertStatusCodeSemProblema(response);
		String responseBody = response.getBody().asString();
		
		pausinha();
		AuditoriaRequisicao ultimaAuditoria = auditoriaRequisicaoRepository.findTopByOrderByInicioOperacaoDesc();
		assertThat(ultimaAuditoria).isNotNull();
		
		assertThat(ultimaAuditoria.getNomeAplicacaoCliente()).isEqualTo(CLIENT_APPLICATION_NAME_PARA_TESTES);
		LocalDateTime agoraDePouco = DateUtils.agora().minusMinutes(1);
	    assertThat(ultimaAuditoria.getInicioOperacao()).isAfter(agoraDePouco);
	    assertThat(ultimaAuditoria.getFimOperacao()).isAfter(agoraDePouco);
	    assertThat(ultimaAuditoria.getInicioOperacao()).isBefore(ultimaAuditoria.getFimOperacao());
	    assertThat(ultimaAuditoria.getMetodoHttp()).isEqualTo("POST");
	    assertThat(ultimaAuditoria.getRecurso()).isEqualTo("/api/albuns");
	    assertThat(ultimaAuditoria.getQueryString()).isNull();
	    assertThat(ultimaAuditoria.getOperacao()).isEqualTo("POST /api/albuns");
	    assertThat(ultimaAuditoria.getCorpoRequisicao()).isNotBlank(); // TODO melhorar
	    assertThat(ultimaAuditoria.getCorpoResposta()).isEqualTo(responseBody);
	    assertThat(ultimaAuditoria.getStatusCodeResposta()).isEqualTo("201");
	    assertThat(ultimaAuditoria.getErroInterno()).isFalse();
	    assertThat(ultimaAuditoria.getDescricaoErroInterno()).isBlank();
	    assertThat(ultimaAuditoria.getIpConsumidorServico()).isEqualTo("127.0.0.1");
	    assertThat(ultimaAuditoria.getTraceId()).matches(TraceIdManager.TRACE_ID_REGEX);
	    
	    // verifica efetivação da transação de negócio
	    List<Album> albuns = albumRepository.findAll();
	    assertThat(albuns).isNotEmpty();
	    Album album = albuns.get(0);
	    assertThat(album.getTitulo()).isEqualTo(novoAlbum.getTitulo());
	    assertThat(album.getArtista()).isEqualTo(novoAlbum.getArtista());
	}
	
	@Test
	void requisicoes_com_erro_interno_devem_ser_auditadas_mas_transacao_de_negocio_deve_ter_rollback() {
		
		albumRepository.deleteAll();
		
		NovoAlbum novoAlbum = massa.album1();
		Response response = requisicao().body(novoAlbum).queryParam("causarErroInterno", true)
				.contentType(ContentType.JSON).post(urlAlbuns());
		assertStatusCode500(response);
		String responseBody = response.getBody().asString();
		
		pausinha();
		AuditoriaRequisicao ultimaAuditoria = auditoriaRequisicaoRepository.findTopByOrderByInicioOperacaoDesc();
		assertThat(ultimaAuditoria).isNotNull();
		
		System.out.println("  >>> Registro de auditoria com erro interno: " + ultimaAuditoria);
		
		assertThat(ultimaAuditoria.getNomeAplicacaoCliente()).isEqualTo(CLIENT_APPLICATION_NAME_PARA_TESTES);
		LocalDateTime agoraDePouco = DateUtils.agora().minusMinutes(1);
	    assertThat(ultimaAuditoria.getInicioOperacao()).isAfter(agoraDePouco);
	    assertThat(ultimaAuditoria.getFimOperacao()).isAfter(agoraDePouco);
	    assertThat(ultimaAuditoria.getInicioOperacao()).isBefore(ultimaAuditoria.getFimOperacao());
	    assertThat(ultimaAuditoria.getMetodoHttp()).isEqualTo("POST");
	    assertThat(ultimaAuditoria.getRecurso()).isEqualTo("/api/albuns");
	    assertThat(ultimaAuditoria.getQueryString()).isEqualTo("causarErroInterno=true");
	    assertThat(ultimaAuditoria.getOperacao()).isEqualTo("POST /api/albuns");
	    assertThat(ultimaAuditoria.getCorpoRequisicao()).isNotBlank(); // TODO melhorar
	    assertThat(ultimaAuditoria.getCorpoResposta()).isEqualTo(responseBody);
	    assertThat(ultimaAuditoria.getStatusCodeResposta()).isEqualTo("500");
	    assertThat(ultimaAuditoria.getErroInterno()).isTrue();
	    assertThat(ultimaAuditoria.getDescricaoErroInterno()).isNotBlank();
	    assertThat(ultimaAuditoria.getIpConsumidorServico()).isEqualTo("127.0.0.1");
	    assertThat(ultimaAuditoria.getTraceId()).matches(TraceIdManager.TRACE_ID_REGEX);
	    
	    // verifica rollback na transação de negócio
	    List<Album> albuns = albumRepository.findAll();
	    assertThat(albuns).isEmpty();
	}
	
	

	private void pausinha() {
		// Pausinha pra dar tempo do flush do banco (ou algo assim).
		// TODO Essa pausa não deveria ser necessária, tem como removê-la?
		// Problema é que sem ela, os efeitos da transação parecem invisíveis logo
		// imediatamente após a chamada REST.
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			throw new IllegalStateException("Impossível", e);
		}
	}

	@Test
	void requisicoes_com_erro_na_gravacao_da_auditoria_devem_logar_erro_mas_persistir_transacao_de_negocio() {
	
		albumRepository.deleteAll();
		
		NovoAlbum novoAlbum = massa.album1();
		Response response = requisicao().body(novoAlbum).queryParam("causarErroNaAuditoria", true)
				.contentType(ContentType.JSON).post(urlAlbuns());
		assertStatusCodeSemProblema(response);
		
		// Como houve erro na auditoria, não teremos registros de auditoria.
		pausinha();
	    List<AuditoriaRequisicao> auditorias = auditoriaRequisicaoRepository.findAll();
	    assertThat(auditorias).isEmpty();
	    System.out.println("  >>> O log da aplicação deve conter o registro de auditoria que não pôde ser auditado no banco de dados.");
	    
		// Contudo, quando há o erro de auditoria, a transação de negócio já foi efetivada
	    // verifica efetivação da transação de negócio
	    List<Album> albuns = albumRepository.findAll();
	    assertThat(albuns).isNotEmpty();
	    Album album = albuns.get(0);
	    assertThat(album.getTitulo()).isEqualTo(novoAlbum.getTitulo());
	    assertThat(album.getArtista()).isEqualTo(novoAlbum.getArtista());	    
	}

}
