package chastra.controlederajadas.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

/**
 * Padrão controle de rajadas
 *
 */
class LimpadorDeCorpoRequisicaoTest {

	private LimpadorDeCorpoRequisicao limpador = new LimpadorDeCorpoRequisicao();
	
	@Test
	void remover_campos_nao_chaves() {
		
		String corpoRequisicao = "{ \"idArtista\":12354, \"quantidadeFans\":12654 }";
		String corpoRequisicaoComCamposChaves = limpador.removerCamposQueDevemSerIgnorados(corpoRequisicao);
		assertThat(corpoRequisicaoComCamposChaves).contains("idArtista").doesNotContain("quantidadeFans");
	}

}
