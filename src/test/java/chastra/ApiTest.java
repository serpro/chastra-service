package chastra;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.transaction.TestTransaction;

import chastra.http.Headers;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import jakarta.persistence.EntityManager;

public abstract class ApiTest {
	
	static final String CLIENT_APPLICATION_NAME_PARA_TESTES = "chastra-testes";
	
	private static final String ALBUNS_URL = "http://localhost:%s/chastra/api/albuns";
	private static final String ALBUM_POR_ID_URL = "http://localhost:%s/chastra/api/albuns/{idAlbum}";
	private static final String IMAGEM_CAPA_URL = "http://localhost:%s/chastra/api/albuns/{idAlbum}/imagem-capa";
	private static final String ERRO_INTERNO_URL = "http://localhost:%s/chastra/api/erros/erro-interno";
	private static final String ERRO_DE_NEGOCIO_URL = "http://localhost:%s/chastra/api/erros/erro-de-negocio";
	private static final String REQUISICAO_INVALIDA_URL = "http://localhost:%s/chastra/api/erros/requisicao-invalida";
	private static final String HEALTH_ALIVE_URL = "http://localhost:%s/chastra/health/alive";
	private static final String HEALTH_INTEGRATION_URL = "http://localhost:%s/chastra/health/integration";
	private static final String OPERACAO_DEMORADA_URL = "http://localhost:%s/chastra/api/operacao-demorada";
	private static final String OPERACAO_A_URL = "http://localhost:%s/chastra/api/transacao-distribuida/operacao-a";
	private static final String OPERACAO_B_URL = "http://localhost:%s/chastra/api/transacao-distribuida/operacao-b";
	private static final String OPERACAO_FEATURE_TOGGLES = "http://localhost:%s/chastra/api/feature-toggles";
	private static final String OPERACAO_ATIVACAO_FEATURE_TOGGLE = "http://localhost:%s/chastra/api/feature-toggles/ativacao";
	
    @Autowired
    private EntityManager entityManager;
    
    @LocalServerPort
    private String porta;
    
    String urlAlbumPorId() {
    	return url(ALBUM_POR_ID_URL);
    }
    
    String urlAlbuns() {
    	return url(ALBUNS_URL);
    }
    
    String urlImagemCapa() {
    	return url(IMAGEM_CAPA_URL);
    }

    String urlErroInterno() {
    	return url(ERRO_INTERNO_URL);
    }

    String urlOperacaoA() {
    	return url(OPERACAO_A_URL);
    }

    String urlOperacaoB() {
    	return url(OPERACAO_B_URL);
    }

    String urlErroDeNeogcio() {
    	return url(ERRO_DE_NEGOCIO_URL);
    }

    String urlRequisicaoInvalida() {
    	return url(REQUISICAO_INVALIDA_URL);
    }
    
    String urlHealthAlive() {
    	return url(HEALTH_ALIVE_URL);
    }
    
    String urlHealthIntegration() {
    	return url(HEALTH_INTEGRATION_URL);
    }
    
    String urlOperacaoDemorada() {
    	return url(OPERACAO_DEMORADA_URL);
    }
    
	String urlFeatureToggles() {
		return url(OPERACAO_FEATURE_TOGGLES);
	}

	String urlAtivarFeatureToggle() {
		return url(OPERACAO_ATIVACAO_FEATURE_TOGGLE);
	}

	private String url(String baseUrl) {
		return String.format(baseUrl, porta);
	}

	void assertStatusCodeSemProblema(Response response) {
		String errorMessage = "Status code foi " + response.statusCode() + " com body " + response.getBody().asString();
		assertThat(response.getStatusCode()).as(errorMessage).satisfiesAnyOf(
				statusCode -> assertThat(statusCode).isEqualTo(200),
				statusCode -> assertThat(statusCode).isEqualTo(201)
		);
	}

	void assertStatusCode200(Response response) {
		assertStatusCode(response, 200);
	}

	void assertStatusCode422(Response response) {
		assertStatusCode(response, 422);
	}

	void assertStatusCode401(Response response) {
		assertStatusCode(response, 401);
	}

	void assertStatusCode400(Response response) {
		assertStatusCode(response, 400);
	}

	void assertStatusCode500(Response response) {
		assertStatusCode(response, 500);
	}

	void assertStatusCode(Response response, int statusCode) {
		String errorMessage = "Status code foi " + response.statusCode() + " com body " + response.getBody().asString();
		assertThat(response.getStatusCode()).as(errorMessage).isEqualTo(statusCode);
	}

	@SuppressWarnings("rawtypes")
	List asList(Response response, Class<? extends Object> clazz) {
		return response.getBody().jsonPath().getList(".", clazz);
	}
	
	RequestSpecification requisicao() {
		return given().header(Headers.CLIENT_APPLICATION_NAME, CLIENT_APPLICATION_NAME_PARA_TESTES);
	}
	
    protected void commit() {
        entityManager.flush();
        TestTransaction.flagForCommit();
        TestTransaction.end();
        TestTransaction.start();
    }

}
