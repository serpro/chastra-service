package chastra;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import chastra.album.api.AlbumJson;
import io.restassured.response.Response;

/**
 * Demonstra o padrão fields.
 *
 */
@Disabled("Nossa implementação desse padão não funciona no Spring Boot 3.")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class DemonstracaoPadraoFieldsTest extends ApiTest {

	@Test
	@SuppressWarnings("unchecked")
	void uso_de_query_param_fields_deve_omitir_campos_na_resposta() {
		
		// sem usar o fields
		
		Response response = requisicao().get(urlAlbuns());
		assertStatusCodeSemProblema(response);
		String json = response.getBody().asString();
		json = omiteBinarios(json);
		System.out.println("  >>> Resposta sem usar o fields (volta todos os campos): " + json);
		List<AlbumJson> albums = asList(response, AlbumJson.class);
		
		assertThat(albums).isNotEmpty();
		assertThat(albums.stream()).as("Todos os campos devem estar preenchidos.").allMatch(a -> comTodosOsCampos(a));
		
		// usando o fields
		
		response = requisicao().queryParam("fields", "titulo,artista").get(urlAlbuns());
		assertStatusCodeSemProblema(response);
		System.out.println("  >>> Resposta usando o fields (volta só alguns campos): " + response.getBody().asString());
		albums = asList(response, AlbumJson.class);
		
		assertThat(albums).isNotEmpty();
		assertThat(albums.stream()).as("Apenas alguns campos devem estar preenchidos.").allMatch(a -> soComTituloEArtista(a));
	}

	private String omiteBinarios(String json) {
		// omitindo binários porque a impressão deles buga o console do Eclipse
		return json.replaceAll("imagemCapaBase64\":\".*?\"", "imagemCapaBase64\":\"...omitido pelo teste na impressão...\"");
	}

	private boolean comTodosOsCampos(AlbumJson a) {
		return StringUtils.isNotBlank(a.getTitulo()) && StringUtils.isNotBlank(a.getArtista())
				&& a.getAnoLancamento() != null && StringUtils.isNotBlank(a.getImagemCapaBase64());
	}

	private boolean soComTituloEArtista(AlbumJson a) {
		return StringUtils.isNotBlank(a.getTitulo()) && StringUtils.isNotBlank(a.getArtista())
				&& a.getAnoLancamento() == null && a.getImagemCapaBase64() == null;
	}

}
