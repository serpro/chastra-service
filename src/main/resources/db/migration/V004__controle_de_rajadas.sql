-- Padrão controle de rajadas
create table operacao_em_andamento
(
  id bigserial primary key,                                  
  recurso text,
  metodo_http text,
  corpo_requisicao_com_campos_chaves text,
  data_hora_registro timestamp,
  unique (recurso, metodo_http, corpo_requisicao_com_campos_chaves)
);

alter table auditoria_requisicao add column foi_rajada boolean;
