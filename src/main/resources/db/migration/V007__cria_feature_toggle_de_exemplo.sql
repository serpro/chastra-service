-- Padrão feature toggle

-- Feature toggles são criados via migrations

-- Feature toggle de exemplo:
insert into feature_toggle 
	(funcionalidade, descricao, data_criacao, ligada) 
values 
	('excluir-album', -- chave que será usada no código-fonte
	'Possibilita a exclusão de álbuns via API.', -- documentação, com possível rastro para requisitos 
	'2024-05-08', -- data na qual a migration foi escrita (documentação)
	false);

-- Por padrão, um feature toggle nasce 
-- habilitado nos ambientes local, des e tes
-- e desabilitadas nos ambientes hom e pro:
DO $$
BEGIN
	IF current_user = 'postgres' THEN -- se não é nem hom, nem pro
		update feature_toggle set ligada = true where funcionalidade = 'transferencia-entre-lojas'; 
	END IF;
END $$ LANGUAGE PLPGSQL;
