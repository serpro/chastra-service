-- Padrão auditoria
create table auditoria_requisicao
(
  id bigserial primary key,
  nome_aplicacao_cliente text,
  inicio_operacao timestamp without time zone,
  fim_operacao timestamp without time zone,
  metodo_http text,
  recurso text,
  query_string text,
  operacao text,
  corpo_requisicao text,
  corpo_resposta text,
  status_code_resposta text,
  erro_interno boolean,
  descricao_erro_interno text,
  ip_consumidor_servico text,
  trace_id text
);

create index idx_inicio_operacao__auditoria_requisicao
  on auditoria_requisicao (inicio_operacao);
create index idx_erro_interno__auditoria_requisicao
  on auditoria_requisicao (erro_interno);  
create index idx_nome_aplicacao_cliente__auditoria_requisicao
  on auditoria_requisicao (nome_aplicacao_cliente);  

