create table feature_toggle
(
  id bigserial primary key,
  funcionalidade text,
  descricao text,
  data_criacao date,
  data_hora_ativacao timestamp,
  ligada boolean default false
);
