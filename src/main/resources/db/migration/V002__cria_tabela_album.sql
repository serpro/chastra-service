create table album
(
  id bigserial primary key,
  titulo text,
  artista text,
  ano_lancamento int,
  imagem_capa bytea
);
