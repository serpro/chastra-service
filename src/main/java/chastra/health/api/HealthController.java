package chastra.health.api;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import chastra.health.domain.DatabaseChecker;
import chastra.health.domain.Diagnostico;
import chastra.health.domain.HealthChecker;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.log4j.Log4j2;

/**
 * Padrão health check
 *
 */
@RestController
@RequestMapping(value = "/health", produces = { "text/plain;charset=utf-8" })
@Log4j2
@Tag(name = "Saúde", description = "Endpoint para informações da Saúde do microserviço")
public class HealthController {

	@Autowired
	private HealthChecker healthChecker;
	
	@Autowired
	private DatabaseChecker databaseChecker;

	@GetMapping
	public ResponseEntity<String> checkIntegrationsPorConveniencia(
			@RequestParam(required = false, defaultValue = "false") Boolean forcarErro500AoVerificarAcessoAoServicoRecomendacao,
			@RequestParam(required = false, defaultValue = "false") Boolean forcarErroTimeoutAoVerificarAcessoAoServicoRecomendacao) {
		
		return checkIntegrations(forcarErro500AoVerificarAcessoAoServicoRecomendacao, forcarErroTimeoutAoVerificarAcessoAoServicoRecomendacao);
	}

	@GetMapping("integration")
	public ResponseEntity<String> checkIntegrations(
			@RequestParam(required = false, defaultValue = "false") Boolean forcarErro500AoVerificarAcessoAoServicoRecomendacao,
			@RequestParam(required = false, defaultValue = "false") Boolean forcarErroTimeoutAoVerificarAcessoAoServicoRecomendacao) {

		Diagnostico diagnostico = healthChecker.check(forcarErro500AoVerificarAcessoAoServicoRecomendacao, forcarErroTimeoutAoVerificarAcessoAoServicoRecomendacao);
		Set<String> observacoes = diagnostico.getObservacoes();

		if (diagnostico.isNotOk()) {
			log.error("/health/integration not ok: " + observacoes);
		}

		int statusCode = diagnostico.isOk() ? 200 : 500;
		Map<String, String> diag = new HashMap<>();
		diag.put("ok", Boolean.toString(diagnostico.isOk()));
		diag.put("observacoes", observacoes.toString());
		return new ResponseEntity<>(diag.toString(), HttpStatus.valueOf(statusCode));
	}

	@GetMapping("alive")
	public ResponseEntity<String> checkLiveness() {
		Diagnostico diagnostico = databaseChecker.check();
		int statusCode = diagnostico.isOk() ? 200 : 500;
		
		Map<String, String> diag = new HashMap<>();
		diag.put("ok", Boolean.toString(diagnostico.isOk()));
		diag.put("observacoes", diagnostico.getObservacoes().toString());
		return new ResponseEntity<>(diag.toString(), HttpStatus.valueOf(statusCode));
	}

}
