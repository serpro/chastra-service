package chastra.health.domain;

import jakarta.persistence.EntityManager;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Padrão health check
 *
 */
@Component
public class DatabaseChecker {

    private static final String PROBLEMA_BANCO_DE_DADOS = "Problema ao acessar o banco de dados Postgres.";
    private static final String BANCO_DE_DADOS_OK = "Acesso ao banco de dados Postgres OK.";

    @Autowired
    private EntityManager entityManager;
    
    @Autowired
    private DadosConexaoBanco dadosConexaoBanco;

    public Diagnostico check() {

        Diagnostico diagnostico = new Diagnostico();

        try {
            select1();
            diagnostico.addObservacao(BANCO_DE_DADOS_OK);
        } catch (Exception e) {
            String stackTrace = ExceptionUtils.getStackTrace(e);
            String detalhes = detalhesDoProblema();
            diagnostico.setNotOk(PROBLEMA_BANCO_DE_DADOS + "\n" + detalhes + "\n" + stackTrace);
        }

        return diagnostico;
    }

    private void select1() {
        entityManager.createNativeQuery("select 1").getSingleResult();
        entityManager.close();
    }

    private String detalhesDoProblema() {
        String detalhes = null;
        try {
            if (dadosConexaoBanco != null) {
                detalhes = dadosConexaoBanco.toString();
            }
        } catch (Exception e) {
            detalhes = e.getMessage();
        }
        return detalhes;
    }
    
}
