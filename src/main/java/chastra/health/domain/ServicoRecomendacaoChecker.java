package chastra.health.domain;

import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

/**
 * Faz de conta que invoca o health/alive de um suposto "serviço de recomendação".
 * Padrão health check
 * 
 */
@Log4j2
@Component
class ServicoRecomendacaoChecker extends AbstractExternalServiceChecker {

	private boolean causarErro500;
	private boolean causarErroTimeout;
	
	/**
	 * Argumentos apenas para propósito de testes, não são thread-safe.
	 * 
	 */
	public Diagnostico check(boolean causarErro500, boolean causarErroTimeout) {
		
		this.causarErro500 = causarErro500;
		this.causarErroTimeout = causarErroTimeout;
		return check();
	}
	
	@Override
	public int acessar() {
		
		if (causarErro500) {
			return 500;
		}
		
		if (causarErroTimeout) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				throw new IllegalStateException("Timeout invoking http", e);
			}
			throw new IllegalStateException("Timeout invoking http");
		}
		
		return 200;
	}

	@Override
	public String getServiceName() {
		return "serviço de recomendação";
	}

	@Override
	public String getServiceUrl() {
		return "http://chastra.com/recomendacao/health/alive";
	}

	@Override
	public void logErro(String msg, Exception e) {
		log.error(msg, e);
	}

}
