package chastra.health.domain;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Padrão health check
 *
 */
@Component
public class HealthChecker {
	
	@Autowired
	private DatabaseChecker databaseChecker;
	
	@Autowired
	private ServicoRecomendacaoChecker servicoRecomendacaoChecker;

	public Diagnostico check() {
		return check(false, false);
	}
	
	public Diagnostico check(boolean forcarErro500AoVerificarAcessoAoServicoRecomendacao, boolean forcarErroTimeoutAoVerificarAcessoAoServicoRecomendacao) {
		
		Diagnostico diagnostico = databaseChecker.check();
		diagnostico.agrega(servicoRecomendacaoChecker.check(forcarErro500AoVerificarAcessoAoServicoRecomendacao, forcarErroTimeoutAoVerificarAcessoAoServicoRecomendacao));
		return diagnostico;
	}

}
