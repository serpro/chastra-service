package chastra.health.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * Padrão health check
 *
 */
@Component
@Data
class DadosConexaoBanco {

    @Value("${spring.datasource.username}")
    private String userName;
    
    @Value("${spring.datasource.url}")
    private String connectionUrl;

}
