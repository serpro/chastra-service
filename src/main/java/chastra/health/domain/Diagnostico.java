package chastra.health.domain;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Padrão health check
 *
 */
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
@EqualsAndHashCode
@ToString
public class Diagnostico {

	private boolean ok = true;
	private Set<String> observacoes = new HashSet<>();

	public boolean isOk() {
		return ok;
	}

	@JsonIgnore
	public boolean isNotOk() {
		return !ok;
	}

	public void setNotOk() {
		this.ok = false;
	}
	
	public void setNotOk(String observacao) {
		this.ok = false;
		this.observacoes.add(observacao);
	}
	
	public Set<String> getObservacoes() {
		return observacoes;
	}
	
	public boolean semObservacoes() {
		return observacoes.isEmpty();
	}

	public void setObservacoes(Set<String> observacoes) {
		this.observacoes = observacoes;
	}

	public void addObservacao(String observacao) {
		this.observacoes.add(observacao);
	}

	public void addObservacoes(Set<String> observacoes) {
		this.observacoes.addAll(observacoes);
	}

	public String getUmaObservacao() {
		return observacoes.iterator().next();
	}

	public void agrega(Diagnostico diagnostico) {
		if (!diagnostico.isOk()) {
			this.setNotOk();
		}
		Set<String> observacoes = diagnostico.getObservacoes();
		this.addObservacoes(observacoes);
	}
	
	public String getObservacoesAsString() {
        StringBuilder observacoesAsString = new StringBuilder();
        for (String observacao : this.getObservacoes()) {
            observacoesAsString.append(observacao);
        }
        return observacoesAsString.toString();
    }

}
