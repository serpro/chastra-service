package chastra.health.domain;

import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * Padrão health check
 *
 */
abstract class AbstractExternalServiceChecker {

	public static final String OK = "Acesso ao %s OK. URL: %s.";
	public static final String PROBLEMA = "Problemas ao acessar o serviço %s.";

	/**
	 * 
	 * @return status code retornado pelo /health/alive (ou equivalente) do serviço
	 *         dependente
	 */
	public abstract int acessar();

	public abstract String getServiceName();

	public abstract String getServiceUrl();

	public abstract void logErro(String msg, Exception e);

	public Diagnostico check() {

		Diagnostico diagnostico = new Diagnostico();

		try {
			int httpReturnCode = acessar();
			if (httpReturnCode == 200) {
				String msgOk = String.format(OK, getServiceName(), getServiceUrl());
				diagnostico.addObservacao(msgOk);
			} else {
				String msg = messagemDeProblema(
						String.format(" URL: %s. Return code: %s", getServiceUrl(), httpReturnCode));
				diagnostico.setNotOk(msg);
			}
		} catch (Exception e) {
			logErro("Erro ao verificar " + getServiceName() + ".", e);
			diagnostico.setNotOk();
			String stackTrace = ExceptionUtils.getStackTrace(e);
			String msg = messagemDeProblema("\n" + stackTrace);
			diagnostico.addObservacao(msg);
		}

		return diagnostico;
	}

	public String messagemDeProblema(String complemento) {
		String msg = String.format(PROBLEMA, getServiceName());
		return msg + complemento;
	}

}
