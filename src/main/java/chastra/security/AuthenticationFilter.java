package chastra.security;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;

import chastra.context.ChastraServiceRequestContext;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Uma simples autenticação no contexto de serviços internos que se confiam,
 * muito mais para efeitos de auditoria do que para segurança.
 *
 */
public class AuthenticationFilter implements Filter {
	
	private ChastraServiceRequestContext requestContext;

    public AuthenticationFilter(ApplicationContext applicationContext) {
        requestContext = applicationContext.getBean(ChastraServiceRequestContext.class);
    }

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterchain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;

		String nomeCliente = requestContext.getNomeAplicacaoCliente();
		if (StringUtils.isBlank(nomeCliente)) {
			response.setStatus(401);
			String msg = "Informe o header Client-Application-Name";
			PrintWriter writer = response.getWriter();
			writer.println(msg);
			return;
		}

		filterchain.doFilter(request, response);

	}

	@Override
	public void init(FilterConfig filterconfig) throws ServletException {
		// nada aqui
	}
	
	@Override
	public void destroy() {
		// nada aqui
	}
	
}
