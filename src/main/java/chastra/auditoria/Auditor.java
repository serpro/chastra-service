package chastra.auditoria;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.log4j.Log4j2;

/**
 * Padrão auditoria
 *
 */
@Component
@Log4j2
public class Auditor {

    @Autowired
    private AuditoriaRequisicaoFactory auditoriaRequisicaoFactory;
    
    @Autowired
    private AuditorBancoDeDados auditorBancoDeDados;

    public void auditar() {
    	auditar(false);
    }
    
	public void auditar(boolean causarErroAuditoria) {

		AuditoriaRequisicao registroAuditoria = null;
		try {
			registroAuditoria = auditoriaRequisicaoFactory.createAuditoriaRequisicao();
		} catch (Exception e) {
			log.error("Não consegui auditar no banco de dados.", e);
			return;
		}
		try {
			auditorBancoDeDados.auditar(registroAuditoria, causarErroAuditoria);
		} catch(Exception e) {
			String msg = String.format("Não consegui auditar no banco de dados o seguinte registro de auditoria: %s.", registroAuditoria);
			log.error(msg, e);
			// poderíamos enviar um e-mail para a equipe também
		}
	}

}
