package chastra.auditoria;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Component;

import de.ralleytn.simple.json.JSONArray;
import de.ralleytn.simple.json.JSONObject;

/**
 * Padrão supressão de binários
 *
 */
@Component
public class SupressorDeBinarios {
	
	public static final String MARCA_DE_SUPRESSAO = "CONTEUDO_OMITIDO_NA_AUDITORIA";

	private static final String[] CAMPOS_A_LIMPAR = new String[] {}; // por ora array vazio, mas pode ser útil

	public String suprimirBinarios(String json) {
		
		if (ehLista(json)) {
			return suprimirBinariosDeListaJson(json);
		} else {
			return suprimirBinariosDeObjetoJson(json);
		}
	}

	private boolean ehLista(String json) {
		return json.trim().startsWith("[");
	}
	
	private String suprimirBinariosDeListaJson(String json) {
		try {
			JSONArray jsonArray = new JSONArray(json);
			omitirCampos(jsonArray);
			return jsonArray.toString();
		} catch (Exception e) {
			return json;
		}
	}

	/**
	 * objeto = {...}
	 */
	private String suprimirBinariosDeObjetoJson(String json) {
		
		try {
			JSONObject jsonObject = new JSONObject(json);
			omitirCampos(jsonObject);
			return jsonObject.toString();
		} catch (Exception e) {
			return json;
		}
	}
	
	private void omitirCampos(JSONArray jsonArray) {
		for (int i = 0; i < jsonArray.size(); i++) {  
			Object value = jsonArray.get(i);
			if (value instanceof JSONObject jsonObject) {
				omitirCampos(jsonObject);
			}
			if (value instanceof JSONArray jsonSubArray) {
				omitirCampos(jsonSubArray);
			}
		}
	}

	private void omitirCampos(JSONObject json) {

		for (Object key : json.keySet()) {
			String keyStr = (String) key;
			if (temValor(json, keyStr)) {
				if (deveOmitir(keyStr)) {
					json.put(keyStr, MARCA_DE_SUPRESSAO);
				} else {
					Object value = json.get(keyStr);
					if (value instanceof JSONObject jsonObject) {
						omitirCampos(jsonObject);
					}
					if (value instanceof JSONArray jsonArray) {
						omitirCampos(jsonArray);
					}
				}
			}
		}
	}

	private boolean temValor(JSONObject json, String keyStr) {
		Object valor = json.get(keyStr);
		boolean valorEhString = valor instanceof String;
		return valor != null && (!valorEhString || valorEhString && !"".equals(valor));
	}

	private boolean deveOmitir(String nomeCampo) {
		return nomeCampo.endsWith("Base64") || ArrayUtils.contains(CAMPOS_A_LIMPAR, nomeCampo);
	}

}
