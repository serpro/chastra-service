package chastra.auditoria;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import chastra.context.ChastraServiceRequestContext;
import chastra.traceid.TraceIdManager;

/**
 * Padrões "auditoria", "supressão de binários" e "tracing""
 *
 */
@Component
class AuditoriaRequisicaoFactory {

	@Autowired
	private ChastraServiceRequestContext requestContext;

	@Autowired
	private SupressorDeBinarios supressor;

	public AuditoriaRequisicao createAuditoriaRequisicao() {

		AuditoriaRequisicao auditoria = new AuditoriaRequisicao();

		auditoria.setNomeAplicacaoCliente(requestContext.getNomeAplicacaoCliente());
		auditoria.setInicioOperacao(requestContext.getInicioOperacao());
		auditoria.setFimOperacao(requestContext.getFimOperacao());
		String metodoHttp = requestContext.getMetodoHttp();
		auditoria.setMetodoHttp(metodoHttp);
		String recurso = requestContext.getRecurso();
		auditoria.setRecurso(recurso);
		auditoria.setQueryString(requestContext.getQueryString());
		auditoria.setOperacao(requestContext.getOperacao());
		auditoria.setCorpoRequisicao(supressor.suprimirBinarios(requestContext.getCorpoRequisicao()));
		auditoria.setCorpoResposta(supressor.suprimirBinarios(requestContext.getCorpoResposta()));
		auditoria.setStatusCodeResposta(requestContext.getStatusCodeResposta());
		auditoria.setErroInterno(requestContext.isErroInterno());
		auditoria.setDescricaoErroInterno(requestContext.getDescricaoErroInterno());
		auditoria.setIpConsumidorServico(requestContext.getIpConsumidorServico());
		auditoria.setTraceId(TraceIdManager.getTraceId());

		return auditoria;
	}

}
