package chastra.auditoria;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Padrão auditoria
 *
 */
@Component
class AuditorBancoDeDados {
	
	@Autowired
	private AuditoriaRequisicaoRepository auditoriaRequisicaoRepository;
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void auditar(AuditoriaRequisicao registroAuditoria, boolean causarErroAuditoria) {
		if (causarErroAuditoria) {
			throw new IllegalStateException("Erro causado de propósito durante a transação de auditoria");
		}
		auditoriaRequisicaoRepository.save(registroAuditoria);
	}

}
