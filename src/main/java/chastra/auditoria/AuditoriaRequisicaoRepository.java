package chastra.auditoria;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Padrão auditoria
 *
 */
@Repository
public interface AuditoriaRequisicaoRepository extends JpaRepository<AuditoriaRequisicao, Long> {

	AuditoriaRequisicao findTopByOrderByInicioOperacaoDesc();
	
}
