package chastra.auditoria;

import java.time.LocalDateTime;

import jakarta.persistence.*;

import lombok.Data;

/**
 * Padrão auditoria
 *
 */
@Data
@Entity
@Table(name = "auditoria_requisicao")
@SequenceGenerator(allocationSize = 1, initialValue = 1000, name = "auditoria_requisicao_id_seq", sequenceName = "auditoria_requisicao_id_seq")
public class AuditoriaRequisicao {

	@Id
    @Column
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auditoria_requisicao_id_seq")
    private Long id;

	@Column
	private String nomeAplicacaoCliente;
	
	@Column
    private LocalDateTime inicioOperacao;
	
	@Column
    private LocalDateTime fimOperacao;
	
	@Column
    private String metodoHttp;
	
	@Column
    private String recurso;
	
	@Column
    private String queryString;
	
	@Column
    private String operacao;
	
	@Column
    private String corpoRequisicao;
	
	@Column
    private String corpoResposta;
	
	@Column
    private String statusCodeResposta;
	
	@Column
    private Boolean erroInterno;
	
	@Column
    private String descricaoErroInterno;
	
	@Column
    private String ipConsumidorServico;
	
	@Column
    private String traceId;
	
    /**
     * Padrão controle de rajadas
     *
     */
	@Column
    private Boolean foiRajada = false;

}
