package chastra.controlederajadas.domain;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import lombok.Data;

/**
 * Padrão controle de rajadas
 *
 */
@Entity
@Data
public class OperacaoEmAndamento {

	@Id
    @SequenceGenerator(name = "operacao_em_andamento_id_seq", sequenceName = "operacao_em_andamento_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "operacao_em_andamento_id_seq")
    private Long id;
	
	@Column
	private String recurso;
	
	@Column
	private String metodoHttp;
	
	@Column
	private String corpoRequisicaoComCamposChaves;
	
	@Column
	private LocalDateTime dataHoraRegistro;
	
}
