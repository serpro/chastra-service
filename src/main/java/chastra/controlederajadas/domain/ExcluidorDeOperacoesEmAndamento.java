package chastra.controlederajadas.domain;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import chastra.utils.DateUtils;
import lombok.extern.java.Log;

/**
 * Padrão controle de rajadas
 *
 */
@Component
@EnableScheduling
@Log
public class ExcluidorDeOperacoesEmAndamento {

    private static final long CINCO_MINUTOS = 5 * 60 * 1000;

    @Autowired
    private OperacaoEmAndamentoService service;

    @Scheduled(fixedDelay = CINCO_MINUTOS)
    public void excluirOperacoesEmAndamentoQueJaDeveriamTerSidoExcluidas() {
        
        log.info("Apagando registros de operação em andamento que já deveriam ter sido excluídos.");
        
        LocalDateTime quatroMinutosAtras = DateUtils.agora().minus(4, ChronoUnit.MINUTES);
        
        List<OperacaoEmAndamento> registrosQueJaDeveriamTerSidoExcluidos = service
                .consultarPorDataHoraRegistroAntesDe(quatroMinutosAtras);
        
        for (OperacaoEmAndamento operacaoEmAndamento : registrosQueJaDeveriamTerSidoExcluidos) {
            service.excluir(operacaoEmAndamento.getId());
        }
    }

}
