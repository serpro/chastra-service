package chastra.controlederajadas.domain;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import chastra.controlederajadas.infra.OperacaoEmAndamentoRepository;
import chastra.utils.DateUtils;

/**
 * Padrão controle de rajadas
 *
 */
@Service
public class OperacaoEmAndamentoService {

	@Autowired
	private OperacaoEmAndamentoRepository repository;
	
	@Autowired
	private LimpadorDeCorpoRequisicao limpadorDeCorpoRequisicao;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void inserir(String recurso, String metodoHttp, String corpoRequisicao) {

		String corpoRequisicaoComCamposChaves = limpadorDeCorpoRequisicao
				.removerCamposQueDevemSerIgnorados(corpoRequisicao);

		if (semCamposChaves(recurso, corpoRequisicaoComCamposChaves)) {
			return; // se não possui campos chaves, então não tem que controlar rajadas
		}
		
		OperacaoEmAndamento operacaoEmAndamento = new OperacaoEmAndamento();
		operacaoEmAndamento.setRecurso(recurso);
		operacaoEmAndamento.setMetodoHttp(metodoHttp);
		operacaoEmAndamento.setCorpoRequisicaoComCamposChaves(corpoRequisicaoComCamposChaves);
		operacaoEmAndamento.setDataHoraRegistro(DateUtils.agora());

		repository.save(operacaoEmAndamento);
	}

	private boolean semCamposChaves(String recurso, String corpoRequisicaoComCamposChaves) {

		// pressuposto: IDs possuem dígitos e nomes de recursos não possuem dígitos
		boolean semIdentificadorNoPath = StringUtils.getDigits(recurso).isEmpty();
		
		boolean semCamposChavesNoBody = StringUtils.isBlank(corpoRequisicaoComCamposChaves);
		
		return semIdentificadorNoPath && semCamposChavesNoBody;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void excluir(String recurso, String metodoHttp, String corpoRequisicao) {

		String corpoRequisicaoComArgumentosChaves = limpadorDeCorpoRequisicao.removerCamposQueDevemSerIgnorados(corpoRequisicao);
		repository.deleteByRecursoAndMetodoHttpAndCorpoRequisicaoComCamposChavesAllIgnoreCase(recurso, metodoHttp, corpoRequisicaoComArgumentosChaves);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void excluir(Long id) {
		repository.deleteById(id);
	}

	public List<OperacaoEmAndamento> consultarPorDataHoraRegistroAntesDe(LocalDateTime dataHora) {
		return repository.findByDataHoraRegistroBefore(dataHora);
	}

}
