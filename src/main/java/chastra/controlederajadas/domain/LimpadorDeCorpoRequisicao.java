package chastra.controlederajadas.domain;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import de.ralleytn.simple.json.JSONObject;

/**
 * Padrão controle de rajadas
 *
 */
@Component
public class LimpadorDeCorpoRequisicao {
	
	private static final List<String> PADROES_DE_CAMPOS_CHAVES = Arrays.asList("id[A-Z].*", "numero[s]?[A-Z].*");
	
	public String removerCamposQueDevemSerIgnorados(String corpoRequisicao) {
		try {
			JSONObject jsonObject = new JSONObject(corpoRequisicao);
			removerCampos(jsonObject);
			return jsonObject.toString();
		} catch (Exception e) {
			return corpoRequisicao;
		}
	}

	private void removerCampos(JSONObject json) {

		json.keySet().removeIf(key -> !ehCampoChave(key));

		for (Object key : json.keySet()) {
			Object value = json.get(key.toString());
			if (value instanceof JSONObject) {
				removerCampos((JSONObject) value);
			}
		}
	}

	private boolean ehCampoChave(Object key) {
		return PADROES_DE_CAMPOS_CHAVES.stream().anyMatch(pattern -> key.toString().matches(pattern));
	}

}
