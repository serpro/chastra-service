package chastra.controlederajadas.infra;

import java.io.IOException;
import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;

import chastra.auditoria.Auditor;
import chastra.context.ChastraServiceRequestContext;
import chastra.controlederajadas.domain.OperacaoEmAndamentoService;
import chastra.utils.DateUtils;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletResponse;

/**
 * Padrão controle de rajadas
 *
 */
public class ControleDeRajadasFilter implements Filter {

	private ChastraServiceRequestContext requestContext;
	private OperacaoEmAndamentoService operacaoEmAndamentoService;
	private Auditor auditor;

	public ControleDeRajadasFilter(ApplicationContext applicationContext) {
		requestContext = applicationContext.getBean(ChastraServiceRequestContext.class);
		operacaoEmAndamentoService = applicationContext.getBean(OperacaoEmAndamentoService.class);
		auditor = applicationContext.getBean(Auditor.class);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		String recurso = requestContext.getRecurso();
		String metodoHttp = requestContext.getMetodoHttp();
		String corpoRequisicao = requestContext.getCorpoRequisicao();

		if (!deveProcessar(recurso, metodoHttp)) {
			chain.doFilter(request, response);
			return;
		}

		try {

			operacaoEmAndamentoService.inserir(recurso, metodoHttp, corpoRequisicao);

			chain.doFilter(request, response);

			operacaoEmAndamentoService.excluir(recurso, metodoHttp, corpoRequisicao);

		} catch (Exception e) {

			String msg = "O controle de rajadas detectou que já existe uma operação em andamento para este recurso ("
					+ recurso + "). Favor aguardar o término do processamento.";

			HttpServletResponse responseHttp = (HttpServletResponse) response;
			responseHttp.getOutputStream().write(msg.getBytes());
			responseHttp.setStatus(422);

			auditarRajada(msg);
		}

	}

	private void auditarRajada(String mensagem) {

		LocalDateTime agora = DateUtils.agora();
		requestContext.setFimOperacao(agora);
		requestContext.setCorpoResposta(mensagem);
		requestContext.setFoiRajada(true);

		auditor.auditar();
	}

	private boolean deveProcessar(String recurso, String metodoHttp) {
		return StringUtils.equalsIgnoreCase(metodoHttp, "POST") || StringUtils.equalsIgnoreCase(metodoHttp, "DELETE");
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// vazio - nada a fazer aqui
	}

	@Override
	public void destroy() {
		// vazio - nada a fazer aqui
	}

}
