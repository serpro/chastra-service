package chastra.controlederajadas.infra;

import org.springframework.stereotype.Repository;

import chastra.controlederajadas.domain.OperacaoEmAndamento;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Padrão controle de rajadas
 *
 */
@Repository
public interface OperacaoEmAndamentoRepository extends JpaRepository<OperacaoEmAndamento, Long> {

	public void deleteByRecursoAndMetodoHttpAndCorpoRequisicaoComCamposChavesAllIgnoreCase(String recurso, 
			String metodoHttp, String corpoRequisicaoComCamposChaves);

    public List<OperacaoEmAndamento> findByDataHoraRegistroBefore(LocalDateTime dataHora);

}
