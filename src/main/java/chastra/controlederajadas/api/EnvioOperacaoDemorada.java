package chastra.controlederajadas.api;

import lombok.Data;

/**
 * Padrão controle de rajadas
 *
 */
@Data
public class EnvioOperacaoDemorada {

	private Long idArtista;
	private Integer quantidadeFans;
	
}
