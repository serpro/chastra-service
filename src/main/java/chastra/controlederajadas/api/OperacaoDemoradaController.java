package chastra.controlederajadas.api;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * Padrão controle de rajadas
 *
 */
@RestController
@RequestMapping(value = "/api/operacao-demorada")
@Tag(name = "Operação demorada", description = "Endpoint para testar o controle de rajadas")
public class OperacaoDemoradaController {

	@PostMapping
	public void operacaoDemorada(@RequestBody EnvioOperacaoDemorada envio) {
	
		demorar();
	}

	private void demorar() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			throw new IllegalStateException("Exceção no sleep. Nunca deveria acontecer!", e);
		}		
	}
	
}
