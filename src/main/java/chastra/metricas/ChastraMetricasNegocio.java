package chastra.metricas;

import org.springframework.stereotype.Component;

import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;

@Log
@Component
@AllArgsConstructor
public class ChastraMetricasNegocio {

    private CompositeMeterRegistry meterRegistry;

    /**
     * Padrão monitoração para o negócio
     *
     */
    public void incMetricaAlbunsPorArtista(String nomeArtista) {
        try {
            String nomeArtistaFormatado = nomeArtista.toLowerCase().trim();
            meterRegistry.counter("contador_albuns_por_artista", "nome", nomeArtistaFormatado).increment();
        } catch (Exception e) {
            log.severe("Erro ao registrar métrica " + e.getMessage());
        }
    }

}
