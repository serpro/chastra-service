package chastra.metricas;

import java.io.IOException;

import org.springframework.context.ApplicationContext;

import chastra.context.ChastraServiceRequestContext;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;

/**
 * Padrão tracing
 *
 */
public class PrometheusFilter implements Filter {

	private ChastraServiceRequestContext requestContext;
	private ChastraMetricasTracing chastraMetricas;
	
	public PrometheusFilter(ApplicationContext applicationContext) {
		this.requestContext = applicationContext.getBean(ChastraServiceRequestContext.class);
		this.chastraMetricas = new ChastraMetricasTracing(applicationContext);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		chain.doFilter(request, response);
		
		chastraMetricas.incRequisicoes(requestContext.getOperacao(), requestContext.getNomeAplicacaoCliente(),
				requestContext.getNomeDoServicoDeBorda(), requestContext.getStatusCodeResposta());
	}

}
