package chastra.metricas;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;

import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import lombok.extern.java.Log;

/**
 * Padrão tracing
 *
 */
@Log
public class ChastraMetricasTracing {

	private CompositeMeterRegistry meterRegistry;

	public ChastraMetricasTracing(ApplicationContext applicationContext) {
		try {
			this.meterRegistry = applicationContext.getBean(CompositeMeterRegistry.class);
		} catch (NoSuchBeanDefinitionException e) {
			log.warning("Não consegui injetar " + CompositeMeterRegistry.class
					+ ". Tudo bem se estivermos no ambiente de testes.");
		}
	}

	public void incRequisicoes(String operacao, String nomeAplicacaoCliente, String nomeServicoDeBorda,
			String statusCodeResposta) {
		if (meterRegistry != null) {
			try {
				meterRegistry.counter("contador_requisicoes", "operacao", operacao, "nomeAplicacaoCliente",
						nomeAplicacaoCliente, "nomeServicoDeBorda", nomeServicoDeBorda, "statusCodeResposta",
						statusCodeResposta).increment();
			} catch (Exception e) {
				log.severe("Erro ao registrar métrica " + e.getMessage());
			}
		}
	}

}
