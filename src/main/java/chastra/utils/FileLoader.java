package chastra.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;

public class FileLoader {

    private String filePath;

    public FileLoader(String filePath) {
        this.filePath = filePath;
    }

    public File getFile() throws IOException {
        if (isAbsolutPath()) {
            return getFileFromAbsolutPath();
        } else {
            return getFileFromClassPath();
        }
    }

    private File getFileFromAbsolutPath() {
        return new File(filePath);
    }

    private File getFileFromClassPath() throws IOException {
        URL url = this.getClass().getClassLoader().getResource(filePath);
        if (url == null) {
            fail();
        }
        String absolutPath = url.getFile();
        return new File(absolutPath);
    }

    public InputStream getInputStream() throws IOException {
        if (isAbsolutPath()) {
            return getInputStreamFromAbsolutPath();
        } else {
            return getInputStreamFromClassPath();
        }
    }

    private boolean isAbsolutPath() {
        return filePath.startsWith("/");
    }

    private InputStream getInputStreamFromAbsolutPath() throws IOException {
        InputStream is = null;
        try {
            is = new FileInputStream(filePath);
        } catch (FileNotFoundException e) {
            fail();
        }
        return is;
    }

    private InputStream getInputStreamFromClassPath() throws IOException {
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(filePath);
        if (is == null) {
            fail();
        }
        return is;
    }

    public byte[] getBytes() throws IOException {
        File file = getFile();
		return Files.readAllBytes(file.toPath());
    }

    private void fail() throws IOException {
        String msg = filePath + " does not exist.";
        throw new IOException(msg);
    }

}