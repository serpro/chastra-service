package chastra.utils;

import org.apache.commons.codec.binary.Base64;

public class BytesUtils {
	
	private BytesUtils() {
		
	}

    public static byte[] base64ToBytes(String stringBase64) {
        if (Base64.isBase64(stringBase64)) {
            return Base64.decodeBase64(stringBase64);
        } else {
            return new byte[] {};
        }
    }
    
    public static String bytesToBase64(byte[] bytes) {
        return Base64.encodeBase64String(bytes);
    }
    
}
