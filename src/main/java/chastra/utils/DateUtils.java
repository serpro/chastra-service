package chastra.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class DateUtils {

	public static LocalDateTime agora() {
		// especificação de ZoneId evita problemas com servidor com horário configurado incorretamente
		return LocalDateTime.now(ZoneId.of("America/Buenos_Aires"));
	}

}
