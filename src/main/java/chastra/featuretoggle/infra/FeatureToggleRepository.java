package chastra.featuretoggle.infra;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import chastra.featuretoggle.domain.FeatureToggle;

/**
 * Padrão feature toggle
 *
 */
@Repository
public interface FeatureToggleRepository extends JpaRepository<FeatureToggle, Long> {

	FeatureToggle getByFuncionalidade(String funcionalidade);
	
}
