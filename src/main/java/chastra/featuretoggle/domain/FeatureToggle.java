package chastra.featuretoggle.domain;

import java.time.LocalDate;
import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

/**
 * Padrão feature toggle
 *
 */
@Data
@Entity
public class FeatureToggle {

	@Id
    private Long id;

    @Column
    private String funcionalidade;

    @Column
    private String descricao;

    @Column
    private LocalDate dataCriacao;

    @Column
    private LocalDateTime dataHoraAtivacao;

    @Column
    private Boolean ligada;

}
