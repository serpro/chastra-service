package chastra.featuretoggle.domain;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import chastra.exception.ErroDeNegocio;
import chastra.featuretoggle.infra.FeatureToggleRepository;
import chastra.utils.DateUtils;

/**
 * Padrão feature toggle
 *
 */
@Service
public class FeatureToggleService {

	@Autowired
	private FeatureToggleRepository repository; 
	
	public boolean isFeatureToggleLigada(String funcionalidade) {
		
		FeatureToggle featureToggle = repository.getByFuncionalidade(funcionalidade);
		return featureToggle != null && featureToggle.getLigada();
	}

	/**
	 * Padrão feature toggle via swagger
	 */
	public List<FeatureToggle> findAll() {
		
		return repository.findAll();
	}

	/**
	 * Padrão feature toggle via swagger
	 */
	public void ativar(String nomeFuncionalidade) {
        
		FeatureToggle featureToggle = repository.getByFuncionalidade(nomeFuncionalidade);
        if (featureToggle == null) {
            throw new ErroDeNegocio("Não existe feature toggle '" + nomeFuncionalidade + "'.");
        }
        if (featureToggle.getLigada() != Boolean.TRUE) {
            featureToggle.setLigada(true);
            featureToggle.setDataHoraAtivacao(DateUtils.agora());
            repository.save(featureToggle);
        }		
	}

}
