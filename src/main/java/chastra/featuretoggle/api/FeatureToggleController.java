package chastra.featuretoggle.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import chastra.featuretoggle.domain.FeatureToggle;
import chastra.featuretoggle.domain.FeatureToggleService;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
 * Padrão feature toggle via swagger
 */
@RestController
@RequestMapping(value = "/api/feature-toggles")
@Tag(name = "Feature Toggle", description = "Obs: essas operações seriam expostas apenas em um serviço interno.")
public class FeatureToggleController {

    @Autowired
    private FeatureToggleService featureToggleService;
    
    @GetMapping
    @Tag(name = "Consulta feature toggle")
    public List<FeatureToggle> consultaFeatureToggle() {

        return featureToggleService.findAll();
    }
    
    @PostMapping("/ativacao")
    @Tag(name = "Ativar feature toggle")
    @Transactional
    public void ativarFeatureToggle(
            @RequestParam String nomeFuncionalidade) {

        featureToggleService.ativar(nomeFuncionalidade);
    }
    
}
