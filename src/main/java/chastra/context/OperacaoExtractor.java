package chastra.context;

import org.apache.commons.lang3.StringUtils;

/**
 * Padrão auditoria
 *
 */
class OperacaoExtractor {

	/**
	 * Para um POST na url /albuns/156647790, retorna "POST /albuns".
	 *  
	 * É essa forma normalizada do recurso, a "operação", que
	 * queremos que seja exibida no Grafana. Essa forma normalizada também é útil
	 * para as consultas à auditoria (evitar uso de "like" no recurso).
	 * 
	 * Pressupostos: 
	 * * identificador do recurso possui pelo menos um dígito
	 * * segmentos da URI que não são identificadores não possuem dígitos
	 * 
	 */
	public String getOperacaoFrom(String metodoHttp, String recurso) {

		String[] trechosUrl = StringUtils.splitByWholeSeparatorPreserveAllTokens(recurso, "/");

		StringBuilder recursoNormalizado = new StringBuilder();
		for (String trecho : trechosUrl) {
			if (StringUtils.isNotBlank(trecho) && StringUtils.isAlpha(StringUtils.remove(trecho, "-"))) {
				recursoNormalizado.append("/");
				recursoNormalizado.append(trecho);
			}
		}

		return metodoHttp + " " + recursoNormalizado.toString();
	}
	
}
