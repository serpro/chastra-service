package chastra.context;

import jakarta.servlet.ReadListener;
import jakarta.servlet.ServletInputStream;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletRequestWrapper;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Padrão auditoria
 * 
 * http://www.oodlestechnologies.com/blogs/How-to-create-duplicate-object-of-httpServletRequest-object
 *
 * https://en.wikipedia.org/wiki/Infinite_(Stratovarius_album)
 * 
 * https://www.google.com.br/search?q=infinity+mijinion&client=firefox-b&dcr=0&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiswKfbprzZAhXBDZAKHRa-CCUQ_AUICigB&biw=988&bih=536
 */
class ContentInfiniteReadsHttpRequestWrapper extends HttpServletRequestWrapper {
    
    private String _body;
 
    public ContentInfiniteReadsHttpRequestWrapper(HttpServletRequest request) throws IOException {
        super(request);
        _body = "";
        BufferedReader bufferedReader = request.getReader();           
        String line;
        while ((line = bufferedReader.readLine()) != null){
            _body += line;
        }
    }
 
    @Override
    public ServletInputStream getInputStream() throws IOException {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(_body.getBytes());
        return new ServletInputStream() {
            public int read() throws IOException {
                return byteArrayInputStream.read();
            }

			@Override
			public boolean isFinished() {
				return false;
			}

			@Override
			public boolean isReady() {
				return false;
			}

			@Override
			public void setReadListener(ReadListener arg0) {
			}
        };
    }
 
    @Override
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new InputStreamReader(this.getInputStream()));
    }
}