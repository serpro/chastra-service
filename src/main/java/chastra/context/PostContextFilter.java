package chastra.context;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.IOException;
import java.time.LocalDateTime;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.web.util.ContentCachingResponseWrapper;

import chastra.auditoria.Auditor;
import chastra.utils.DateUtils;

// https://code.i-harness.com/en/q/202e7eb
// https://stackoverflow.com/questions/39935190/contentcachingresponsewrapper-produces-empty-response

/**
 * Padrão auditoria
 *
 */
public class PostContextFilter implements Filter {

    private ChastraServiceRequestContext requestContext;
    private Auditor auditor;
    
    public PostContextFilter(ApplicationContext applicationContext) {
        this.requestContext = applicationContext.getBean(ChastraServiceRequestContext.class);
        this.auditor = applicationContext.getBean(Auditor.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(
                (HttpServletResponse) servletResponse);

        try {
            chain.doFilter(request, responseWrapper);
        } catch (Exception e) {
            // intencionalmente vazio: eh pra auditar (abaixo) mesmo que ocorra algum erro
        }

        LocalDateTime agora = DateUtils.agora();
        requestContext.setFimOperacao(agora);

        String corpoResposta = IOUtils.toString(responseWrapper.getContentInputStream(), UTF_8);
        requestContext.setCorpoResposta(corpoResposta);
        
        requestContext.setStatusCodeResposta(String.valueOf(responseWrapper.getStatus()));

        responseWrapper.copyBodyToResponse();
        
        String causarErroAuditoria = request.getParameter("causarErroNaAuditoria");

        auditar(causarErroAuditoria);
    }

    private void auditar(String causarErroAuditoriaStr) {
    	boolean causarErroAuditoria = Boolean.parseBoolean(causarErroAuditoriaStr);
		auditor.auditar(causarErroAuditoria);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // intencionalmente vazio, comentário só pra deixar o linter feliz
    }

    @Override
    public void destroy() {
        // intencionalmente vazio, comentário só pra deixar o linter feliz
    }

}
