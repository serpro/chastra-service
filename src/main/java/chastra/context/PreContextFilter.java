package chastra.context;

import java.io.IOException;
import java.time.LocalDateTime;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;

import com.google.common.io.CharStreams;

import chastra.http.Headers;
import chastra.utils.DateUtils;

/**
 * Padrão auditoria
 *
 */
public class PreContextFilter implements Filter {

    private ChastraServiceRequestContext requestContext;
	private OperacaoExtractor operacaoExtractor = new OperacaoExtractor();


    public PreContextFilter(ApplicationContext applicationContext) {
        requestContext = applicationContext.getBean(ChastraServiceRequestContext.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
            throws IOException, ServletException {

        HttpServletRequest requestOriginal = ((HttpServletRequest) servletRequest);
        ContentInfiniteReadsHttpRequestWrapper request = new ContentInfiniteReadsHttpRequestWrapper(requestOriginal);

        HttpServletResponse response = (HttpServletResponse) servletResponse;

        LocalDateTime agora = DateUtils.agora();
        requestContext.setInicioOperacao(agora);

        String recurso = request.getServletPath();
        requestContext.setRecurso(recurso);

        String queryString = request.getQueryString();
        requestContext.setQueryString(queryString);

        String metodoHttp = request.getMethod();
        requestContext.setMetodoHttp(metodoHttp);
        
        String operacao = operacaoExtractor.getOperacaoFrom(metodoHttp, recurso);
        requestContext.setOperacao(operacao);

        String corpoRequisicao = corpoDo(request);
        requestContext.setCorpoRequisicao(corpoRequisicao);

        String ipConsumidorServico = request.getHeader("X-Forwarded-For");
        if (StringUtils.isBlank(ipConsumidorServico)) {
            ipConsumidorServico = request.getRemoteAddr();
        }
        requestContext.setIpConsumidorServico(ipConsumidorServico);
        
        String clientChainRecebido = request.getHeader(Headers.CLIENT_CHAIN);
        requestContext.setClientChainRecebido(clientChainRecebido);
        
        String nomeCliente = request.getHeader(Headers.CLIENT_APPLICATION_NAME);
        requestContext.setNomeAplicacaoCliente(nomeCliente);

        chain.doFilter(request, response);

    }

    private String corpoDo(HttpServletRequest request) throws IOException {
        return CharStreams.toString(request.getReader());
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // intencionalmente vazio. comentário só pra deixar o Sonar feliz.
    }

    @Override
    public void destroy() {
        // intencionalmente vazio. comentário só pra deixar o Sonar feliz.
    }

}
