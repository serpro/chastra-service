package chastra.context;

import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import lombok.Data;

/**
 * Padrão auditoria
 *
 */
@Data
@Component
@RequestScope
public class ChastraServiceRequestContext {

	private String nomeAplicacaoCliente;
	private String clientChainRecebido;
    private LocalDateTime inicioOperacao;
    private LocalDateTime fimOperacao;
    private String metodoHttp;
    private String recurso;
    private String queryString;
    private String operacao;
    private String corpoRequisicao;
    private String corpoResposta;
    private String statusCodeResposta;
    private boolean erroInterno = false;
    private String descricaoErroInterno;
    private String ipConsumidorServico;
    
    /**
     * Padrão controle de rajadas
     *
     */
    private Boolean foiRajada = false;

	public String getClientChainASerEnviado() {
		return StringUtils.isNotBlank(clientChainRecebido) ? clientChainRecebido + ";" + nomeAplicacaoCliente : nomeAplicacaoCliente;
	}
	
	public String getNomeDoServicoDeBorda() {
		return StringUtils.isNotBlank(clientChainRecebido) ? clientChainRecebido.split(";")[0] : nomeAplicacaoCliente;
	}

}
