package chastra;

import org.springdoc.core.customizers.OperationCustomizer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.HandlerMethod;

import chastra.exception.ErroDeNaoEncontrado;
import chastra.exception.ErroDeNegocio;
import chastra.exception.ErroInterno;
import chastra.exception.MensagemDeErro;
import chastra.exception.MensagemDeErroInterno;
import chastra.exception.RequisicaoInvalida;
import chastra.http.Headers;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;

/**
 * Padrão swagger
 *
 */
@Configuration
public class ChastraServiceSwaggerConfig {

    private String ambiente;

    public ChastraServiceSwaggerConfig(@Value("${ambiente}") String ambiente) {
        this.ambiente = ambiente;
    }

    @Bean
    public OpenAPI springShopOpenAPI() {

        return new OpenAPI()
                .addSecurityItem(new SecurityRequirement().addList(Headers.CLIENT_APPLICATION_NAME))
                .components(
                        new Components()
                                .addSecuritySchemes(Headers.CLIENT_APPLICATION_NAME,
                                        new SecurityScheme()
                                                .name(Headers.CLIENT_APPLICATION_NAME)
                                                .type(SecurityScheme.Type.APIKEY)
                                                .in(SecurityScheme.In.HEADER)
                                                .description("Informe o header Client-Application-Name")
                                )
                )
                .externalDocs(
                        new ExternalDocumentation()
                                .description("chastra-service - Gitlab")
                                .url("https://gitlab.com/serpro/chastra-service")
                )
                .info(new Info()
                        .title("Chastra Service - ambiente " + ambiente)
						.description(
								"Projeto demonstrando os padrões DGTRA para incremento de capacidade operacional de serviços (menos acionamentos e acionamentos atendidos mais rapidamente)"));

    }
    
	@Bean
	public OperationCustomizer operationCustomizer() {
		return (Operation operation, HandlerMethod handlerMethod) -> {
			operation.getResponses().addApiResponse("400",
					createApiResponse("Requisição inválida", schemaMensagemRequisicaoInvalida()));
			operation.getResponses().addApiResponse("404",
					createApiResponse("Erro de não encontrado", schemaMensagemErroDeNaoEncontrado()));
			operation.getResponses().addApiResponse("422",
					createApiResponse("Erro de negócio", schemaMensagemErroDeNegocio()));
			operation.getResponses().addApiResponse("500",
					createApiResponse("Erro interno", schemaMensagemErroInterno()));
			return operation;
		};
	}    
	
	private ApiResponse createApiResponse(String description, Schema<?> schema) {

		ApiResponse response = new ApiResponse();
		response.setDescription(description);

		Content content = new Content();
		MediaType mediaType = new MediaType();
		mediaType.setSchema(schema);
		content.addMediaType("application/json", mediaType);

		response.setContent(content);

		return response;
	}	
	
	private Schema<?> schemaMensagemRequisicaoInvalida() {
		Schema<RequisicaoInvalida> errorSchema = new Schema<>();
		MensagemDeErro exemplo = MensagemDeErro.requisicaoInvalida()
				.comErro("Mensagem para o desenvolvedor do sistema cliente explicando o problema.").build();
		errorSchema.example(exemplo);
		return errorSchema;
	}

	private Schema<?> schemaMensagemErroDeNegocio() {
		Schema<ErroDeNegocio> errorSchema = new Schema<>();
		MensagemDeErro exemplo = MensagemDeErro.erroDeNegocio()
				.comMensagemParaUsuarioFinal("Mensagem para o usuário final explicando o problema.").build();
		errorSchema.example(exemplo);
		return errorSchema;
	}

	private Schema<?> schemaMensagemErroInterno() {
		Schema<ErroInterno> errorSchema = new Schema<>();
		MensagemDeErroInterno exemplo = new MensagemDeErroInterno();
		errorSchema.example(exemplo);
		return errorSchema;
	}
	
	private Schema<?> schemaMensagemErroDeNaoEncontrado() {
		Schema<ErroDeNaoEncontrado> errorSchema = new Schema<>();
		MensagemDeErro exemplo = MensagemDeErro.naoEncontrado()
				.comMensagemParaUsuarioFinal("Mensagem para o usuário final explicando o que é que não foi encontrado.").build();
		errorSchema.example(exemplo);
		return errorSchema;
	}
}