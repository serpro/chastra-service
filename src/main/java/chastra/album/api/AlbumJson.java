package chastra.album.api;

import java.util.Arrays;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.tomcat.util.codec.binary.Base64;

import chastra.album.domain.Album;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AlbumJson {

	@Schema(description = "ID do álbum")
	private Long id;

	@Schema(description = "Título do Album")
	private String titulo;

	@Schema(description = "Nome do Artista")
	private String artista;

	@Schema(description = "Ano de lançamento")
	private Integer anoLancamento;

	@Schema(description = "Imagem da capa em base 64 até 180kb")
	private String imagemCapaBase64;

	public static List<AlbumJson> from(List<Album> albuns) {
		return albuns.stream().map(AlbumJson::from).toList();
	}

	public static AlbumJson from(Album album) {

		String imagemCapaBase64 = Base64.encodeBase64String(album.getImagemCapa());
		return new AlbumJson(album.getId(), album.getTitulo(), album.getArtista(), album.getAnoLancamento(), imagemCapaBase64);
	}

	// não regerar automaticamente!
	@Override
	public String toString() {
		String hash = imagemCapaBase64 == null ? "null" : Integer.toString(Arrays.hashCode(imagemCapaBase64.getBytes()));
		return new ToStringBuilder(this).append("titulo", titulo).append("artista", artista)
				.append("anoLancamento", anoLancamento)
				.append("hash(imagemCapaBase64)", hash).toString();
	}

}
