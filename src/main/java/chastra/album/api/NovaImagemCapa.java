package chastra.album.api;

import java.util.Arrays;

import org.apache.commons.lang3.builder.ToStringBuilder;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NovaImagemCapa {

	public static final String BASE64_PATTERN = "[A-Za-z0-9+/]+";

	@NotNull
	@Pattern(regexp = BASE64_PATTERN)
	private String imagemCapaBase64;

	@Override
	public String toString() {
		String hash = imagemCapaBase64 == null ? "null"
				: Integer.toString(Arrays.hashCode(imagemCapaBase64.getBytes()));
		return new ToStringBuilder(this).append("hash(imagemCapaBase64)", hash).toString();
	}

}
