package chastra.album.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import chastra.album.domain.Album;
import chastra.album.domain.AlbumCriado;
import chastra.album.domain.AlbumService;
import chastra.album.domain.NovoAlbum;
import chastra.exception.RequisicaoInvalida;
import chastra.featuretoggle.domain.FeatureToggleService;
import chastra.utils.BytesUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;

@RestController
@RequestMapping(value = "/api/albuns")
@Tag(name = "Álbum", description = "Endpoint para controle de Álbuns")
public class AlbumController {

	@Autowired
	private AlbumService albumService;
	
	@Autowired
	private FeatureToggleService featureToggleService;
	
	@Value("${ambiente}")
	private String ambiente;

	@GetMapping
	public List<AlbumJson> consultarAlbums() {

		List<Album> albuns = albumService.getAlbuns();
		return AlbumJson.from(albuns);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	@Transactional
	public AlbumCriado postarAlbum(@Valid @RequestBody NovoAlbum novoAlbum,
			@RequestParam(required = false) Boolean causarErroInterno,
			@RequestParam(required = false) Boolean causarErroNaAuditoria) {

		AlbumCriado albumCriado = albumService.novoAlbum(novoAlbum);

		if (Boolean.TRUE.equals(causarErroInterno)) {
			causarErroInterno();
		}
		
		return albumCriado;
	}

	private int causarErroInterno() {
		try {
			return 1 / 0;
		} catch (Exception e) {
			throw new IllegalStateException("Esse foi um erro interno lançado de propósito.", e);
		}
	}

	@PutMapping("/{idAlbum}/imagem-capa")
	@Transactional
	@ResponseStatus(HttpStatus.OK)
	public void postarImagemCapa(@PathVariable Long idAlbum,
										 @Valid @RequestBody NovaImagemCapa novaImagemCapa) {

		byte[] imagemCapa = BytesUtils.base64ToBytes(novaImagemCapa.getImagemCapaBase64());
		albumService.atualizarImagemCapa(idAlbum, imagemCapa);
	}
	
	@DeleteMapping("/{idAlbum}")
	@Transactional
	@ResponseStatus(HttpStatus.OK)
	public void excluirAlbum(@PathVariable Long idAlbum) {

		// Padrão feature toggle
		if (featureToggleService.isFeatureToggleLigada("excluir-album")) {
			albumService.excluirAlbum(idAlbum);
		} else {
			throw new RequisicaoInvalida("Funcionalidade ainda não disponível no ambiente " + ambiente + ".");
		}
	}

}
