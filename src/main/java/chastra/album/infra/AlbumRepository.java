package chastra.album.infra;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import chastra.album.domain.Album;

/**
 * Padrão controle de rajadas
 *
 */
@Repository
public interface AlbumRepository extends JpaRepository<Album, Long> {

	List<Album> findAllByTituloAndArtista(String titulo, String artista);

}
