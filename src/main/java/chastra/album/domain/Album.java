package chastra.album.domain;

import chastra.utils.DateUtils;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDateTime;
import java.util.Arrays;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Album {
	
    @Id
    @SequenceGenerator(name = "album_id_seq", sequenceName = "album_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "album_id_seq")
    private Long id;

    @Column
	private String titulo;
	
    @Column
	private String artista;
	
    @Column
	private Integer anoLancamento;

    @Column
	private byte[] imagemCapa;
    
    @Column
    private LocalDateTime dataHoraInclusao = DateUtils.agora();

	public Album(Long id, String titulo, String artista, Integer anoLancamento, byte[] imagemCapa) {
		this.id = id;
		this.titulo = titulo;
		this.artista = artista;
		this.anoLancamento = anoLancamento;
		this.imagemCapa = imagemCapa;
	}
    
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("titulo", titulo).append("artista", artista)
				.append("anoLancamento", anoLancamento).append("hash(imagemCapa)", Arrays.hashCode(imagemCapa))
				.append("dataHoraInclusao", dataHoraInclusao).toString();
	}
	
}
