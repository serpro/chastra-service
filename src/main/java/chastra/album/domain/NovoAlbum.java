package chastra.album.domain;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NovoAlbum {

	@NotEmpty
	private String titulo;

	@NotEmpty
	private String artista;

	@NotNull
	@Min(1900)
	private Integer anoLancamento;

	public static Builder comTitulo(String titulo) {
		return new Builder(titulo);
	}

	public static class Builder {
		
		private String titulo;
		private String artista;
		private Integer anoLancamento;
		
		public Builder(String titulo) {
			this.titulo = titulo;
		}
		public Builder comArtista(String artista) {
			this.artista = artista;
			return this;
		}
		public Builder comAnoLancamento(Integer anoLancamento) {
			this.anoLancamento = anoLancamento;
			return this;
		}
		
		public NovoAlbum build() {
			return new NovoAlbum(titulo, artista, anoLancamento);
		}
	}	
	
}
