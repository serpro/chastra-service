package chastra.album.domain;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import chastra.album.infra.AlbumRepository;
import chastra.exception.ErroDeNaoEncontrado;
import chastra.exception.ErroDeNegocio;
import chastra.metricas.ChastraMetricasNegocio;
import chastra.utils.FileLoader;

@Service
public class AlbumService {

	@Autowired
	private AlbumRepository albumRepository;

	@Autowired
	private ChastraMetricasNegocio chastraMetricas;

	public List<Album> getAlbuns() {

		List<Album> albums = new ArrayList<>();
		albums.add(new Album(1L, "Iron Maiden", "Iron Maiden", 1980, capa("iron-maiden.png")));
		albums.add(new Album(2L, "Brave New World", "Iron Maiden", 2000, capa("brave-new-world.png")));
		albums.add(new Album(3L, "Da Lama ao Caos", "Chico Science & Nação Zumbi", 1994, capa("da-lama-ao-caos.png")));
		albums.add(new Album(4L, "Algens Cry", "Angra", 1993, capa("angels-cry.png")));
		albums.add(new Album(5L, "Holy Land", "Angra", 1996, capa("holy-land.png")));
		return albums;
	}

	private byte[] capa(String nomeArquivo) {
		FileLoader fileLoader = new FileLoader("static/capas/" + nomeArquivo);
		try {
			return fileLoader.getBytes();
		} catch (IOException e) {
			throw new IllegalArgumentException("Não consegui achar a capa de álbum " + nomeArquivo, e);
		}
	}

	public AlbumCriado novoAlbum(NovoAlbum novoAlbum) {

		Optional<Album> albumJaCadastrado = controleDeIdempotencia(novoAlbum);
		if (albumJaCadastrado.isPresent()) {
			return new AlbumCriado(albumJaCadastrado.get().getId());
		}

		Album album = new Album();
		album.setTitulo(novoAlbum.getTitulo());
		album.setArtista(novoAlbum.getArtista());
		album.setAnoLancamento(novoAlbum.getAnoLancamento());

		albumRepository.save(album);
		
		chastraMetricas.incMetricaAlbunsPorArtista(novoAlbum.getArtista());
		return new AlbumCriado(album.getId());
	}

	public Optional<Album> getByTituloAndArtista(String titulo, String artista) {

		List<Album> albunsJaCadastrados = albumRepository.findAllByTituloAndArtista(titulo, artista);

		if (albunsJaCadastrados.isEmpty()) {
			return Optional.empty();
		}

		if (albunsJaCadastrados.size() > 1) {
			throw new IllegalStateException(
					"Nunca deveria acontecer, mais de um álbum cadastrado com mesmo título para o mesmo artista");
		}

		Album albumJaCadastrado = albunsJaCadastrados.get(0);

		return Optional.of(albumJaCadastrado);
	}

	/**
	 * 
	 * @return album já cadastrado para o mesmo título e artista
	 */
	private Optional<Album> controleDeIdempotencia(NovoAlbum novoAlbum) {

		String titulo = novoAlbum.getTitulo();
		String artista = novoAlbum.getArtista();

		Optional<Album> albumJaCadastrado = getByTituloAndArtista(titulo, artista);

		if (albumJaCadastrado.isPresent()) {
			verificarValoresEnviadosVsValoresJaCadastrados(novoAlbum, albumJaCadastrado.get());
		}

		return albumJaCadastrado;
	}

	private void verificarValoresEnviadosVsValoresJaCadastrados(NovoAlbum novoAlbum, Album albumJaCadastrado) {

		String titulo = novoAlbum.getTitulo();
		String artista = novoAlbum.getArtista();

		if (!novoAlbum.getAnoLancamento().equals(albumJaCadastrado.getAnoLancamento())) {
			String msg = String.format(
					"Envio duplicado de cadastro de álbum '%s' de artista '%s' possui ano lançamento (%d) divergente do que consta no álbum já cadastrado (%d). Portanto, este segundo envio foi desconsiderado.",
					titulo, artista, novoAlbum.getAnoLancamento(), albumJaCadastrado.getAnoLancamento());
			throw new ErroDeNegocio(msg);
		}
	}

	public void atualizarImagemCapa(Long idAlbum, byte[] imagemCapa) {

		Album albumJaCadastrado = albumRepository.findById(idAlbum)
				.orElseThrow(() -> new ErroDeNaoEncontrado("Álbum " + idAlbum + " não cadastrado."));
		albumJaCadastrado.setImagemCapa(imagemCapa);
	}

	public void excluirAlbum(Long idAlbum) {

		albumRepository.deleteById(idAlbum);
	}

}
