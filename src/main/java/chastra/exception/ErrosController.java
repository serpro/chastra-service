package chastra.exception;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/erros")
@Tag(name = "Erros", description = "Endpoint para simular a captura de erros")
public class ErrosController {

    @GetMapping("erro-interno")
    public int lancarErroInterno() {
    	try {
    		return 1 / 0;
    	} catch (Exception e) {
    		throw new IllegalStateException("Esse foi um erro interno lançado de propósito.", e);
    	}
    }
    
    @GetMapping("erro-de-negocio")
    public void lancarErroDeNegocio() {
    	throw new ErroDeNegocio("Esse foi um erro de negócio lançado de propósito.");
    }
    
    @GetMapping("requisicao-invalida")
    public void lancarRequisicaoInvalida() {
    	throw new RequisicaoInvalida("Esse foi um erro de requisição inválida lançado de propósito.");
    }

}
