package chastra.exception;

import java.time.LocalDateTime;

import chastra.traceid.TraceIdManager;
import chastra.utils.DateUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
public class MensagemDeErroInterno {

	private final String titulo = "Erro Interno";
	private final LocalDateTime dataHora = DateUtils.agora();
	private final String traceId = TraceIdManager.getTraceId();
	private final String erro = "Ocorreu um erro inesperado às " + dataHora
			+ ", tente mais tarde. Se o erro persistir, contate o administrador do sistema informando o trace id: "
			+ traceId + ".";

}
