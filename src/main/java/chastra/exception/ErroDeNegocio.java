package chastra.exception;

public class ErroDeNegocio extends RuntimeException {

    private static final long serialVersionUID = -4462123500075918887L;

	public ErroDeNegocio(String message) {
        super(message);
    }

    public ErroDeNegocio(String message, Throwable cause) {
        super(message, cause);
    }

    
}
