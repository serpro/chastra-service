package chastra.exception;

import chastra.context.ChastraServiceRequestContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
@Log4j2
public class ChastraServiceExceptionHandler {
	
    @Autowired
    private ChastraServiceRequestContext requestContext;

	@ExceptionHandler(ErroDeNegocio.class)
	public ResponseEntity<MensagemDeErro> erroDeNegocio(ErroDeNegocio e, HttpServletRequest request) {

		MensagemDeErro msg = MensagemDeErro.erroDeNegocio().comMensagemParaUsuarioFinal(e.getMessage()).build();

		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY.value()).body(msg);
	}
	
	@ExceptionHandler(ErroDeNaoEncontrado.class)
	public ResponseEntity<MensagemDeErro> erroDeNaoEncontrado(ErroDeNaoEncontrado e, HttpServletRequest request) {

		MensagemDeErro msg = MensagemDeErro.naoEncontrado().comMensagemParaUsuarioFinal(e.getMessage()).build();

		return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(msg);
	}
	
	@ExceptionHandler(RequisicaoInvalida.class)
	public ResponseEntity<MensagemDeErro> requisicaoInvalida(RequisicaoInvalida e, HttpServletRequest request) {

		MensagemDeErro msg = MensagemDeErro.requisicaoInvalida().comErro(e.getMessage()).build();

		return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msg);
	}
	
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<MensagemDeErroInterno> runtimeException(RuntimeException e, HttpServletRequest request) {

        log.error("Erro inesperado: " + e.getMessage(), e);

        requestContext.setErroInterno(true);
        requestContext.setDescricaoErroInterno(ExceptionUtils.getStackTrace(e));

        MensagemDeErroInterno msg = new MensagemDeErroInterno();

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR.value()).body(msg);
    }
    
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<MensagemDeErro> handleBeanValidationViolation(MethodArgumentNotValidException e,
            HttpServletRequest request) {

    	Map<String, String> violacoes = new HashMap<>();
        e.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            violacoes.put(fieldName, errorMessage);
        });

        MensagemDeErro msg = MensagemDeErro.requisicaoInvalida().comErro(violacoes.toString()).build();

        return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msg);
    }
    
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<MensagemDeErro> handleMissingQueryParam(MissingServletRequestParameterException e,
            HttpServletRequest request) {
 
        MensagemDeErro msg = MensagemDeErro.requisicaoInvalida().comErro(e.getMessage()).build();
 
        return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msg);
    }
    
	@ExceptionHandler(ConstraintViolationException.class)
	public ResponseEntity<MensagemDeErro> handleBeanValidationViolation(ConstraintViolationException e, HttpServletRequest request) {

		// método invocado quando @Pattern de um query param é violado
		
		String detalhe = e.getMessage().replaceAll("get([A-Z])", "$1");

		MensagemDeErro msg = MensagemDeErro.requisicaoInvalida().comErro(detalhe).build();

		return ResponseEntity.status(HttpStatus.BAD_REQUEST.value()).body(msg);
	}

}
