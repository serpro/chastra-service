package chastra.exception;

public class ErroInterno extends RuntimeException {

	private static final long serialVersionUID = 2061718913262747081L;

	private final String mensagemParaUsuarioFinal;
	
	public ErroInterno(String msg) {
        super(msg);
        this.mensagemParaUsuarioFinal = null;
    }
	
	public ErroInterno(String msg, Throwable cause) {
		super(msg, cause);
		this.mensagemParaUsuarioFinal = null;
	}
	
	public ErroInterno(String msg, String mensagemParaUsuarioFinal, Throwable cause) {
		super(msg, cause);
		this.mensagemParaUsuarioFinal = mensagemParaUsuarioFinal;
	}

	public String getMensagemParaUsuarioFinal() {
		return mensagemParaUsuarioFinal;
	}
	
}
