package chastra.exception;

import java.time.LocalDateTime;

import chastra.utils.DateUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
public class MensagemDeErro {

	private final String titulo;
	private final LocalDateTime dataHora = DateUtils.agora();
	private final String erro;
	private final String mensagemParaUsuarioFinal;
	
	public MensagemDeErro(String titulo, String erro, String mensagemParaUsuarioFinal) {
		this.titulo = titulo;
		this.erro = erro;
		this.mensagemParaUsuarioFinal = mensagemParaUsuarioFinal;
	}
	
	public static Builder erroDeNegocio() {
		return new Builder("Erro de negócio");
	}

	public static Builder naoEncontrado() {
		return new Builder("Recurso não encontrado");
	}

	public static Builder requisicaoInvalida() {
		return new Builder("Requisição inválida");
	}

	public static class Builder {
		
		private String titulo;
		private String erro;
		private String mensagemParaUsuarioFinal;
		
		public Builder(String titulo) {
			this.titulo = titulo;
		}

		public Builder comErro(String erro) {
			this.erro = erro;
			return this;
		}
		
		public Builder comMensagemParaUsuarioFinal(String mensagemParaUsuarioFinal) {
			this.mensagemParaUsuarioFinal = mensagemParaUsuarioFinal;
			return this;
		}
		
		public MensagemDeErro build() {
			return new MensagemDeErro(titulo, erro, mensagemParaUsuarioFinal);
		}
	}
	
}
