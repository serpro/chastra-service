package chastra.exception;

public class RequisicaoInvalida extends RuntimeException {

    private static final long serialVersionUID = 4899547427044575408L;

	public RequisicaoInvalida(String message) {
        super(message);
    }

    public RequisicaoInvalida(String message, Throwable cause) {
        super(message, cause);
    }

}
