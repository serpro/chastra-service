package chastra.exception;

public class ErroDeNaoEncontrado extends RuntimeException {

	private static final long serialVersionUID = 8725264394936791400L;

	public ErroDeNaoEncontrado(String message) {
        super(message);
    }

}
