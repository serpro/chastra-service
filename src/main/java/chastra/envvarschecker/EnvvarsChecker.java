package chastra.envvarschecker;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Padrão envvars checker
 *
 */
@Component
class EnvvarsChecker {
	
	private static final List<String> IGNORE = List.of("LOGGING_LEVEL");

	private EnvvarsFinder envvarsFinder;
	private EnvvarsEnvironment environment;
	
	@Autowired
	public EnvvarsChecker(EnvvarsFinder envvarsFinder, EnvvarsEnvironment environment) {
		this.envvarsFinder = envvarsFinder;
		this.environment = environment;
	}

	public List<String> envvarsNaoPreenchidas() {
		
		List<String> envvarsNaoPreenchidas = new ArrayList<>();
		
		List<String> envvars = envvarsFinder.find();
		for (String envvar : envvars) {
			if (!IGNORE.contains(envvar)) {
				String varValue = environment.getEnvvar(envvar);
				if (StringUtils.isEmpty(varValue)) {
					envvarsNaoPreenchidas.add(envvar);
				}
			}
		}
		return envvarsNaoPreenchidas;
	}

}
