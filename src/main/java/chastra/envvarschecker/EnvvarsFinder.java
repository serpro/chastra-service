package chastra.envvarschecker;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import chastra.utils.FileLoader;

/**
 * Padrão envvars checker
 *
 */
@Component
class EnvvarsFinder {

	public List<String> find() {

		FileLoader loader = new FileLoader("application.properties");
		String data = null;
		try {
			InputStream inputStream = loader.getInputStream();
			data = IOUtils.toString(inputStream, "UTF-8"); 
		} catch (IOException e) {
			throw new IllegalStateException("Não consegui ler o application.properties", e);
		}
		
		List<String> allMatches = new ArrayList<String>();
		Matcher m = Pattern.compile("\\$\\{([A-Z_]+)[:}]").matcher(data);
		while (m.find()) {
			allMatches.add(m.group(1));
		}

		return allMatches;
	}

	public static void main(String[] args) throws IOException {
		System.out.println(new EnvvarsFinder().find());
	}
}
