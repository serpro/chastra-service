package chastra.envvarschecker;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Padrão envvars checker
 *
 */
@Component
@Order(0)
public class ConfigApplicationListener implements ApplicationListener<ApplicationReadyEvent> {

	private static final Logger logger = LoggerFactory.getLogger(ConfigApplicationListener.class);

	@Value("${ambiente}")
	private String ambiente;
	
	@Value("${server.servlet.context-path}")
	private String urlContext;
	
	@Autowired
	private EnvvarsChecker envvarsChecker;

	@Autowired
	private RestTemplateBuilder restTemplateBuilder;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {

		if (nemHomNemPro()) {
			logger.info("Ambiente não é nem hom, nem pro; portanto pulando verificação de variáveis de ambiente.");
			return;
		}

		List<String> envvarsNaoPreenchidas = envvarsChecker.envvarsNaoPreenchidas();
		if (!envvarsNaoPreenchidas.isEmpty()) {
			String msg = String.format("As seguintes variáveis de ambiente não estão preenchidas: %s. Portanto terminando a aplicação.", envvarsNaoPreenchidas);
			logger.error(msg);
			shutdown();
		} else {
			logger.info("Variáveis de ambiente verificadas, tudo preenchido (tudo certo).");
		}
	}

	private boolean nemHomNemPro() {
	    return !ambiente.substring(0, 3).equalsIgnoreCase("hom") && !ambiente.substring(0, 3).equalsIgnoreCase("pro");
	}

	private void shutdown() {
		String url = String.format("http://localhost:8080/%s/actuator/shutdown", urlContext);
		restTemplateBuilder.build().postForLocation(url, null);
	}

}
