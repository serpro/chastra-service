package chastra.envvarschecker;

import org.springframework.stereotype.Component;

/**
 * Padrão envvars checker
 *
 */
@Component
class EnvvarsEnvironment {

	String getEnvvar(String envvar) {
		return System.getenv(envvar);
	}
	
}
