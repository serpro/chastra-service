package chastra.traceid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;

import chastra.http.Headers;

/**
 * Padrão tracing
 *
 */
public class TraceIdManager {

	/**
	 * Para ser usado nos testes (contudo quem sabe do padrão é a classe que gera o
	 * trace id).
	 */
	public static final String TRACE_ID_REGEX = ".*[0-9a-f]{32}.*";
	
	private TraceIdManager() {

	}

	public static void configurarTraceId(String traceId) {
		if (StringUtils.isNotBlank(traceId)) {
			MDC.put(Headers.TRACE_ID, traceId);
		}
	}

	public static String getTraceId() {
		return MDC.get(Headers.TRACE_ID);
	}
}
