package chastra.traceid;

import chastra.http.Headers;
import jakarta.servlet.ServletRequestEvent;
import jakarta.servlet.ServletRequestListener;
import jakarta.servlet.annotation.WebListener;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Padrão tracing
 *
 */
@WebListener
@Component
public class TraceIdRequestListener implements ServletRequestListener {

	@Override
	public void requestInitialized(ServletRequestEvent event) {

		HttpServletRequest httpRequest = (HttpServletRequest) event.getServletRequest();

		String traceId = httpRequest.getHeader(Headers.TRACE_ID);

		if (clienteNaoMandou(traceId)) {
			traceId = traceIdGeradoInternamente();
		}

		TraceIdManager.configurarTraceId(traceId);
	}

	private String traceIdGeradoInternamente() {
		String uuid = UUID.randomUUID().toString();
		return uuid.replace("-", "");
	}

	private boolean clienteNaoMandou(String traceId) {
		return StringUtils.isBlank(traceId);
	}

	@Override
	public void requestDestroyed(ServletRequestEvent event) {
		// nada a fazer aqui
	}

}
