package chastra;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.github.bohnman.squiggly.Squiggly;
//import com.github.bohnman.squiggly.web.RequestSquigglyContextProvider;
//import com.github.bohnman.squiggly.web.SquigglyRequestFilter;

import chastra.context.PostContextFilter;
import chastra.context.PreContextFilter;
import chastra.controlederajadas.infra.ControleDeRajadasFilter;
import chastra.metricas.PrometheusFilter;
import chastra.security.AuthenticationFilter;

@SpringBootApplication
public class ChastraServiceApplication {

    private static final String URL_API = "/api/*";

    @Autowired
    private ApplicationContext applicationContext;
    
    @Bean
    public FilterRegistrationBean<PreContextFilter> preRequestFilter() {
        FilterRegistrationBean<PreContextFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new PreContextFilter(applicationContext));
        registrationBean.addUrlPatterns(URL_API);
        registrationBean.setOrder(0);
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean<AuthenticationFilter> authenticationFilter() {
        FilterRegistrationBean<AuthenticationFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new AuthenticationFilter(applicationContext));
        registrationBean.addUrlPatterns(URL_API);
        registrationBean.setOrder(1);
        return registrationBean;
    }

    /**
     * Padrão controle de rajadas
     *
     */
    @Bean
    public FilterRegistrationBean<ControleDeRajadasFilter> operacaoEmAndamentoFilter() {
        FilterRegistrationBean<ControleDeRajadasFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new ControleDeRajadasFilter(applicationContext));
        registrationBean.addUrlPatterns(URL_API);
        registrationBean.setOrder(2);
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<PrometheusFilter> prometheusFilter() {
        FilterRegistrationBean<PrometheusFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new PrometheusFilter(applicationContext));
        registrationBean.addUrlPatterns(URL_API);
        registrationBean.setOrder(3);
        return registrationBean;
    }
    
    @Bean
    public FilterRegistrationBean<PostContextFilter> postRequestFilter() {
        FilterRegistrationBean<PostContextFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new PostContextFilter(applicationContext));
        registrationBean.addUrlPatterns(URL_API);
        registrationBean.setOrder(4);
        return registrationBean;
    }

    @Bean
    public MeterRegistry getMeterRegistry() {
        return new CompositeMeterRegistry();
    }
    
//    @Bean
//    public FilterRegistrationBean<SquigglyRequestFilter> squigglyRequestFilter(ObjectMapper objectMapper) {
//
//    	Squiggly.init(objectMapper, new RequestSquigglyContextProvider());
//
//        FilterRegistrationBean<SquigglyRequestFilter> filter = new FilterRegistrationBean<>();
//        filter.setFilter(new SquigglyRequestFilter());
//        filter.setOrder(1);
//        return filter;
//    }
    
	public static void main(String[] args) {
		
		SpringApplication.run(ChastraServiceApplication.class, args);
	}

}
