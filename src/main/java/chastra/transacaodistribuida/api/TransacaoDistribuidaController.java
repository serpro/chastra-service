package chastra.transacaodistribuida.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import chastra.transacaodistribuida.infra.OperacaoBClient;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.java.Log;

/**
 * Padrão tracing
 *
 */
@RestController
@RequestMapping(value = "/api/transacao-distribuida")
@Tag(name = "Transação distribuída", description = "Com operações para testar o rastro distribuído (operação A invoca operação B)")
@Log
public class TransacaoDistribuidaController {

	@Autowired
	private OperacaoBClient operacaoBClient;
	
	@GetMapping("/operacao-a")
	@ResponseStatus(HttpStatus.OK)
	public String operacao1() {
		
		String msg = "Passei pela operação A";
		log.info(msg);
		String retornoOperacao2 = operacaoBClient.invocarOperacaoB();
		return  msg + " / " + retornoOperacao2;
	}
	
	@GetMapping("/operacao-b")
	@ResponseStatus(HttpStatus.OK)
	public String operacao2() {
		
		String msg = "Passei pela operação B";
		log.info(msg);
		return msg;
	}

}
