package chastra.transacaodistribuida.infra;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

import chastra.http.RestClient;

/**
 * Padrão tracing
 *
 */
@Component
public class OperacaoBClient extends RestClient {

	private static final String MENSAGEM_ERRO_INESPERADO = "Erro inesperado ao invocar operação 2";

	@Autowired
	private ServletWebServerApplicationContext webServerAppCtxt;
	
	public String invocarOperacaoB() {
		
		int portaHttp = webServerAppCtxt.getWebServer().getPort(); 
		String url = "http://localhost:" + portaHttp + "/chastra/api/transacao-distribuida/operacao-b";

		try {
			HttpHeaders headers = super.headers();
			HttpEntity<Void> request = new HttpEntity<>(headers);
			return restTemplate().exchange(url, HttpMethod.GET, request, String.class).getBody();
		} catch (HttpStatusCodeException e) {
			tratarErroHttp(e, MENSAGEM_ERRO_INESPERADO);
			return null; // na prática vai acontecer caso o serviço invocado retorne 404
		} catch (Exception e) {
			throw new IllegalStateException(MENSAGEM_ERRO_INESPERADO, e);
		}

	}
}
