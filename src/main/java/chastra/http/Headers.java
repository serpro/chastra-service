package chastra.http;

public class Headers {
	
	private Headers() {}

	public static final String CLIENT_APPLICATION_NAME = "Client-Application-Name";
	
	public static final String TRACE_ID = "Trace-ID";
	
	public static final String CLIENT_CHAIN = "Client-Chain";
	
}
