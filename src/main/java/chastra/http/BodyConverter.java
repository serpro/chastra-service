package chastra.http;

import java.io.IOException;
import java.lang.reflect.Field;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class BodyConverter {

    private ObjectMapper objectMapper;

    private void initMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
            objectMapper.registerModule(new JavaTimeModule());
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        }
    }

    /**
     * Se não houver nenhuma correspondência entre json e atributos da classe,
     * retorna nulo.
     */
    public Object deserialize(String bodyContent, Class<?> clazz) {

        initMapper();

        try {
            Object obj = objectMapper.readValue(bodyContent, clazz);
            if (objetoSohTemNulos(obj, clazz)) {
                return null;
            } else {
                return obj;
            }
        } catch (JsonProcessingException e) {
            return null; // quando bodyContent não é json
        } catch (IOException e) {
            throw new IllegalStateException("Nunca deveria acontecer.", e);
        }
    }

    public String serialize(Object bodyContent) throws JsonProcessingException {
        initMapper();
        return objectMapper.writeValueAsString(bodyContent);
    }

    private boolean objetoSohTemNulos(Object obj, Class<?> clazz) {
        try {
            for (Field f : clazz.getDeclaredFields()) {
                f.setAccessible(true);
                if (f.get(obj) != null) {
                    return false;
                }
            }
        } catch (IllegalArgumentException | IllegalAccessException e) {
            throw new IllegalStateException("Nunca deveria acontecer.", e);
        }
        return true;
    }
    
}
