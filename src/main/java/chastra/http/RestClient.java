package chastra.http;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import chastra.context.ChastraServiceRequestContext;
import chastra.exception.ErroDeNegocio;
import chastra.exception.MensagemDeErro;
import chastra.exception.RequisicaoInvalida;
import chastra.traceid.TraceIdManager;

/**
 * Padrão tracing
 *
 */
public abstract class RestClient {

    public static final String CLIENT_APPLICATION_NAME = "chastra-service";
    private static final Duration QUARENTA_E_CINCO_SEGUNDOS = Duration.of(45, ChronoUnit.SECONDS);
    
	private BodyConverter bodyConverter = new BodyConverter();
    
    @Autowired
    private RestTemplateBuilder restTemplateBuilder;
    
    @Autowired
    private ChastraServiceRequestContext requestContext;
    
    protected HttpHeaders headers() {
        
    	HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        
        // headers de apoiao ao rastro distribuído:
        headers.set(Headers.TRACE_ID, TraceIdManager.getTraceId());
        headers.set(Headers.CLIENT_APPLICATION_NAME, CLIENT_APPLICATION_NAME);
        headers.set(Headers.CLIENT_CHAIN, requestContext.getClientChainASerEnviado());
        
        return headers;
    }
    
    protected RestTemplate restTemplate() {
        return restTemplateBuilder.setConnectTimeout(QUARENTA_E_CINCO_SEGUNDOS).setReadTimeout(QUARENTA_E_CINCO_SEGUNDOS).build();
    }
    
    protected void tratarErroHttp(HttpStatusCodeException e, String mensagemErroInesperado) {
		
    	String body = e.getResponseBodyAsString();
		MensagemDeErro msgDeErro = (MensagemDeErro) bodyConverter.deserialize(e.getResponseBodyAsString(),
				MensagemDeErro.class);
		if (msgDeErro == null) {
			msgDeErro = new MensagemDeErro(null, null, null);
		}

		if (e.getStatusCode().value() == 404) {
			return;
		} else if (e.getStatusCode().value() == 422) {
			if (StringUtils.isNotBlank(msgDeErro.getMensagemParaUsuarioFinal())) {
				throw new ErroDeNegocio(msgDeErro.getMensagemParaUsuarioFinal());
			} else {
				throw new ErroDeNegocio(msgDeErro.getErro());
			}
		} else if (e.getStatusCode().value() == 400) {
			if (StringUtils.isNotBlank(msgDeErro.getMensagemParaUsuarioFinal())) {
				throw new ErroDeNegocio(msgDeErro.getMensagemParaUsuarioFinal());
			} else {
				throw new RequisicaoInvalida(msgDeErro.getErro());
			}
		} else {
			throw new IllegalStateException(
					mensagemErroInesperado + " status code = " + e.getStatusCode().value() + ". body = " + body, e);
		}
	}

}
