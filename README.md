# Chastra Service

Serviço de demonstração de aplicação de padrões de API do Domínio Trânsito do Serpro.

Esses padrões propiciam maior qualidade em termos de manutenibilidade e operabilidade para nossos serviços.

## URLs

(Esta seção aplica o padrão "readme com URLs")

* Swagger: http://localhost:8080/chastra/swagger-ui.html
* Health check: http://localhost:8080/chastra/health
* Métricas: http://localhost:8080/chastra/actuator/prometheus

## Padrões demonstrados

* readme com URLs
* setup doc
* banco local
* swagger
* envvars checker
* supressão de binários
* idempotência
* exception handler
* tracing
* health check
* auditoria
* monitoração para o negócio
* controle de rajadas
* feature toggle
* feature toggle via swagger

Para encontrar as demonstrações e trechos de implementação referentes a cada padrão, basta pesquisar no projeto pelo nome curto do padrão (que são os nomes listados acima).

## Setup e execução local

(Esta seção aplica o padrão "setup doc")

### Requisitos 

* Java 17
* Maven (desenvolvido com a versão 3.8.6)
* Docker (desenvolvido com a versão 20.10.21)
* Docker compose (desenvolvido com a versão 2.12.2)

### Subindo a infra

Basta executar `./run-infra.sh`.

A infra consiste em:

* Uma instância de Postgres (acessível pelo script `psql.sh`).
* Uma instância de Grafana - [http://localhost:3000](http://localhost:3000) login e senha chastra
* Uma instância de Prometheus - [http://localhost:9090](http://localhost:9090)

### Subindo o serviço

Basta executar a classe ChastraServiceApplication (costumamos fazer isso pela IDE).

Obs: compilar e executar com Java 17.

