
# Migração Spring Boot 2.7.14 para 3.1.3 (vai funcionar para qualquer 3.X.X)

## Imports

* de javax.persistence para jakarta.persistence.*;
* de javax.servlet para jakarta.servlet.*
* de javax.validation para jakarta.validation

## Fields

* Problema com o fields [Não resolvido]
* Fields usa HttpServlet do pacote javax e o spring 3 usa do pacote jakarta e dá erro.

## POM.xml

* No lugar de:

```xml
<dependency>
	<!-- padrão "swagger" -->
	<groupId>org.springdoc</groupId>
	<artifactId>springdoc-openapi-ui</artifactId>
	<version>1.7.0</version>
</dependency>
```
substitui por isso aqui:

```xml
<dependency>
	<groupId>org.springdoc</groupId>
	<artifactId>springdoc-openapi-starter-webflux-ui</artifactId>
	<version>2.1.0</version>
</dependency>		
<dependency>
	<groupId>org.springdoc</groupId>
	<artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
	<version>2.2.0</version>
</dependency>
```

## application.properties:

```properties
# OPEN API
application-description=@project.description@
application-version=@project.version@
springdoc.packages-to-scan=chastra.album.api,chastra.health.api,chastra.exception
springdoc.swagger-ui.tryItOutEnabled=true
```
